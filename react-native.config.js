module.exports = {
  dependencies: {
    "react-native-video": {
      platforms: {
        android: {
          sourceDir: "../node_modules/react-native-video/android-exoplayer",
        },
      },
    },
    "react-native-google-cast": {
      platforms: {
        ios: null,
      },
    },
  },
};
