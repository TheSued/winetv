import 'react-native-gesture-handler';
import React from 'react';
import {Dimensions, Platform} from 'react-native';
import {ActiveCompanyContext} from './source/providers/CompanyProvider';
import {AppProvider} from './source/providers';
import Navigator from './source/Navigator';
import defineLanguage from './source/environments/languages/Language';
import Strings_en_GB from './source/environments/languages/Strings-en_GB';
import Strings_it_IT from './source/environments/languages/Strings-it_IT';
import Strings_es_ES from './source/environments/languages/Strings-es_ES';
import Strings_fr_FR from './source/environments/languages/Strings-fr_FR';
import {AppCompanyId, isCustomApp} from './source/environments/AppEnvironment';
import {getUserData} from './source/network/user';
const userLanguage = async () => {
  const companyId = isCustomApp() && AppCompanyId;
  const data = await getUserData(companyId);
  if (data && data.language) {
    if (data.language === 'en') {
      global.strings = Strings_en_GB;
    } else if (data.language === 'it') {
      global.strings = Strings_it_IT;
    } else if (data.language === 'es') {
      global.strings = Strings_es_ES;
    } else if (data.language === 'fr') {
      global.strings = Strings_fr_FR;
    }
  }
};

global.videoEachStep = 9;
global.strings = defineLanguage();

userLanguage();

// without this, if the app starts in landscape orientation the streamview will bug
let param = {
  w: Dimensions.get('screen').width,
  h: Dimensions.get('screen').height,
};
global.screenProp = {
  width: param.w < param.h ? param.w : param.h,
  height: param.w < param.h ? param.h : param.w,
};
// end comment

class App extends React.Component {
  static contextType = ActiveCompanyContext;
  componentDidMount = async () => {
    const companyId = isCustomApp() && AppCompanyId;
    const data = await getUserData(companyId);
    if (data && data.language) {
      if (data.language === 'en') {
        global.strings = Strings_en_GB;
      } else if (data.language === 'it') {
        global.strings = Strings_it_IT;
      } else if (data.language === 'es') {
        global.strings = Strings_es_ES;
      } else if (data.language === 'fr') {
        global.strings = Strings_fr_FR;
      }
    }
  };
  render() {
    return (
      <AppProvider>
        <Navigator />
      </AppProvider>
    );
  }
}

export default App;

// Live slider with live duration / offset from current time, conditional dot red / grey -- Monday
// GoogleCast -- Monday

// ComponentWillMount
// ScrollView + Flatlist

// update to use createAnimatedSwitchNavigator()

// Video element props
// smoothSkinLevel: 0 - 5
// denoise: bool -- mute?
// video: { preset: any number?, profile: 0 - 2 }

// Video presets
// public static final int VIDEO_PPRESET_16X9_270 = 0;
// public static final int VIDEO_PPRESET_16X9_360 = 1;
// public static final int VIDEO_PPRESET_16X9_480 = 2;
// public static final int VIDEO_PPRESET_16X9_540 = 3;
// public static final int VIDEO_PPRESET_16X9_720 = 4;

// Video profiles
// public static final int VIDEO_PROFILE_BASELINE = 0;
// public static final int VIDEO_PROFILE_MAIN = 1;
// public static final int VIDEO_PROFILE_HIGH = 2;
