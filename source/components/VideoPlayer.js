import React from "react";
import ModalSelector from "react-native-modal-selector";
import {
  Alert,
  Animated,
  View,
  Image,
  ImageBackground,
  Platform,
  ActivityIndicator,
  Text,
  TouchableWithoutFeedback,
  Modal,
  Pressable,
  StatusBar,
} from "react-native";

import Slider from "@react-native-community/slider";
import Icon from "react-native-vector-icons/MaterialIcons";
import Video, { TextTrackType } from "react-native-video";
import LinearGradient from "react-native-linear-gradient";

import { PulseIndicator } from "react-native-indicators";

import { ActiveCompanyContext } from "../providers/CompanyProvider";
import Routes from "../environments/Routes";
import * as Network from "../network";
import Colors from "../environments/Colors";
import { FontAwesomeIcon } from "@fortawesome/react-native-fontawesome";
import { faClosedCaptioning } from "@fortawesome/free-solid-svg-icons";
import { faClosedCaptioning as faClosedCaptioningEmpty } from "@fortawesome/free-regular-svg-icons";
import { AppText, Touchable } from "./common";

// Note: VideoPlayer class assume you leave him the control of its size to render the video ratio properly,
// it assume you leave him full width of the screen
// To add margins to video and its overlay, modify the player params in componentWillMount()

// props
// id -- used to update the server on user play / pause
// uri -- uri of the media
// live -- if it's a live or an onDemand video
// started -- if the live is already started, if onDemand == 1
// fullscreen
// viewers -- times the video have been seen

class VideoPlayer extends React.Component {
  static contextType = ActiveCompanyContext;
  tvButtons = [];
  currentButton = 1;

  state = {
    modalVisible: false,
    selectedTextTrackType: "disabled",
    selectedTextTrackValue: 1000,
    videoTextTracks: [],
    pickerTextTracks: [],
    currentTime: 0,
    duration: 0,
    paused: true,
    loadingVideo: true,
    videoError: false,
    castConnected: false,
    casting: false,
    castModal: false,
    isTv: false,

    secondsPlayed: 0,
    intervalId: 0,
    updateIntervalId: 0,
  };
  end = 0;
  action = "";
  video: Video;
  onBuffer = (data) => {
    // this.setState({ duration: data.duration });
  };
  onLoad = (data) => {
    let videoRatio = data.naturalSize.width / data.naturalSize.height;
    let stateSetup = {
      videoWidth: data.naturalSize.width,
      videoHeight: data.naturalSize.height,
      videoWHRatio: videoRatio,

      portraitHeight: global.screenProp.width / videoRatio,
      portraitWidth: global.screenProp.width,
      landscapeHeight: global.screenProp.width,
      landscapeWidth: global.screenProp.width * videoRatio,
      landscapeMarginLeft:
        (global.screenProp.height - global.screenProp.width * videoRatio) / 2,
      landscapeMarginRight:
        (global.screenProp.height - global.screenProp.width * videoRatio) / 2,
    };

    if (Platform.OS == "android") {
      const playerMargin =
        global.screenProp.height - 48 - global.screenProp.width * videoRatio;
      if (playerMargin == 0) {
        // video width == screenWidth
        stateSetup = {
          ...stateSetup,
          landscapeMarginLeft: 0,
          landscapeMarginRight: 0,
        };
      } else if (playerMargin > 0) {
        // else arrange the video to not be under the navbar
        stateSetup = {
          ...stateSetup,
          landscapeMarginLeft: playerMargin / 2,
          landscapeMarginRight: 48 + playerMargin / 2,
        };
      }
    }
    this.video.seek(parseFloat(this.context.playerCurrentTime));
    this.playerOpacityTimer();
    this.setAction();
    this.counter();
    this.setUpdateInterval();
    // this.focusPause();
    this.setState(
      {
        duration: data.duration ? data.duration : 0,
        loadingVideo: false,
        // ...stateSetup,
      },
      () =>
        this.setState({ castModal: this.state.castConnected, paused: false })
    );
  };

  async setAction() {
    this.action = await Network.playVideo(this.props.id, 0);
  }

  focusPause() {
    if (Platform.isTV) {
      if (this.playPauseButton) {
        this.playPauseButton.props.onPress();
        // this.playPauseButton.current.focus();
      } else {
        setTimeout(() => this.focusPause(), 1500);
      }
    }
  }

  setUpdateInterval() {
    let updateIntervalId = setInterval(() => {
      this.updateVideo();
      this.setState({ secondsPlayed: 0 });
    }, 20000);
    this.setState({ updateIntervalId: updateIntervalId });
  }
  stopUpdateInterval() {
    clearInterval(this.state.updateIntervalId);
  }

  onLoadStart = (data) => {
    this.setState({ loadingVideo: true });
  };
  onProgress = (data) => {
    this.props.onProgressTime(data.currentTime);
    this.setState({ currentTime: data.currentTime });
  };
  onEnd = () => {
    this.end = 1;
    this.updateVideo(parseInt(this.state.duration));
    this.setState({ paused: true, currentTime: 0 });
    this.video.seek(0);
    this.stopUpdateInterval();
  };
  onSeek = (data) => console.log("seeking", data);
  onError = (error) => {
    this.setState({ videoError: true, loadingVideo: false });
  };

  seek(seconds) {
    this.video.seek(seconds);
    this.setState({ paused: false });
  }

  async subtitles(id) {
    let subtitles = await Network.getVideoSubtitles(id);
    let newSubtitles = [];
    let newData = [];
    subtitles.map((subtitle, index) => {
      newSubtitles.push({
        index: index,
        title: subtitle.language,
        language: subtitle.language,
        type: TextTrackType.VTT,
        uri: subtitle.subtitles,
      });
      newData.push({
        key: index,
        label: subtitle.language,
      });
    });
    this.setState({
      videoTextTracks: [...newSubtitles],
      pickerTextTracks: [
        {
          key: 1000,
          label: "NONE",
        },
        ...newData,
      ],
    });
  }

  componentWillMount() {
    const { started, uri } = this.props;
    this.overlayOpacity = new Animated.Value(1);
    this.subtitles(this.props.id);
    this.action = "";
    let stateSetup = {
      duration: 0,
      currentTime: 0,
      paused: true,
      loadingVideo: true,
      portraitHeight: (global.screenProp.width / 16) * 9,
      portraitWidth: global.screenProp.width,
      landscapeHeight: global.screenProp.width,
      landscapeWidth: (global.screenProp.width / 9) * 16,
    };

    if (Platform.OS == "android") {
      // TODO: check if softNavBar is present
      const playerMargin =
        global.screenProp.height - 48 - (global.screenProp.width / 9) * 16;
      if (playerMargin == 0) {
        // video width == screenWidth
        stateSetup = {
          ...stateSetup,
          landscapeMarginLeft: 0,
          landscapeMarginRight: 0,
        };
      } else if (playerMargin > 0) {
        // else arrange the video to not be under the navbar
        stateSetup = {
          ...stateSetup,
          landscapeMarginLeft: playerMargin / 2,
          landscapeMarginRight: 48 + playerMargin / 2,
        };
      }
    } else {
      // if iOS or no soft navbar is present
      stateSetup = {
        ...stateSetup,
        landscapeMarginLeft:
          (global.screenProp.height - (global.screenProp.width / 9) * 16) / 2,
        landscapeMarginRight:
          (global.screenProp.height - (global.screenProp.width / 9) * 16) / 2,
      };
    }
    // if the video is a live not started yet, remove the loading feedback
    // from the overlay with the loadingVideo state to false
    this.setState({ ...stateSetup, loadingVideo: !!started });
    // Network.updateVideo(this.props.id, parseInt(this.state.currentTime), this.action)
  }

  componentWillUnmount() {
    this.end = 1;
    this.updateVideo();

    this.stopCounter();
    this.stopUpdateInterval();
  }

  playerOpacityTimer() {
    Animated.timing(this.overlayOpacity, {
      toValue: 1,
      duration: 150,
      useNativeDriver: false,
    }).start(() => {
      if (!this.state.paused) {
        Animated.timing(this.overlayOpacity, {
          toValue: 0,
          duration: 150,
          delay: 3500,
          useNativeDriver: false,
        }).start();
      }
    });
  }

  counter() {
    let intervalId = setInterval(() => {
      this.setState({ secondsPlayed: this.state.secondsPlayed + 1 });
    }, 1000);
    this.setState({ intervalId: intervalId });
  }

  stopCounter() {
    clearInterval(this.state.intervalId);
    this.setState({ secondsPlayed: 0 });
  }

  async playVideo(videoId) {
    this.action = await Network.playVideo(
      videoId,
      parseInt(this.state.currentTime)
    );
  }

  async updateVideo(time) {
    await Network.updateVideo(
      this.props.id,
      time || parseInt(this.state.currentTime || 0),
      this.action,
      this.end,
      this.state.secondsPlayed
    );
  }

  togglePlayer() {
    if (this.state.paused) {
      this.end = 0;
      this.counter();
      this.playVideo(this.props.id);
      this.setUpdateInterval();
      this.setState({ paused: false });
    } else {
      this.end = 1;
      this.stopUpdateInterval();
      this.updateVideo();
      this.stopCounter();
      this.setState({ paused: true });
    }
  }

  convertTime(time, maxTime) {
    let date = new Date(1000 * time);
    if (maxTime > 3600) {
      return date.toISOString().substr(11, 8);
    } else {
      return date.toISOString().substr(14, 5);
    }
  }

  getConvertedVideoDuration() {
    const { duration } = this.state;
    return this.convertTime(duration, duration);
  }

  getConvertedCurrentTime() {
    const { duration, currentTime } = this.state;
    return this.convertTime(currentTime, duration);
  }

  seekingTime(location) {
    this.playerOpacityTimer();
    this.setState({ currentTime: location });
  }

  skipForward() {
    const newTime = this.state.currentTime + 5;

    if (Platform.isTV) {
      this.setState(
        {
          currentTime:
            newTime > this.state.duration ? this.state.duration : newTime,
        },
        () => {
          this.video.seek(this.state.currentTime);
        }
      );
    }
    this.playerOpacityTimer();
    if (this.skipForwardTimer) {
      this.setState(
        {
          currentTime:
            newTime > this.state.duration ? this.state.duration : newTime,
        },
        () => {
          this.video.seek(this.state.currentTime);
          this.skipForwardTimer = false;
        }
      );
    } else {
      this.skipForwardTimer = setTimeout(() => {
        this.skipForwardTimer = false;
      }, 300);
    }
  }

  skipBackward() {
    const newTime = this.state.currentTime - 5;

    if (Platform.isTV) {
      this.setState({ currentTime: newTime >= 0 ? newTime : 0 }, () => {
        this.video.seek(this.state.currentTime);
      });
      return;
    }
    this.playerOpacityTimer();
    if (this.skipBackwardTimer) {
      this.setState({ currentTime: newTime >= 0 ? newTime : 0 }, () => {
        this.video.seek(this.state.currentTime);
        this.skipBackwardTimer = false;
      });
    } else {
      this.skipBackwardTimer = setTimeout(() => {
        this.skipBackwardTimer = false;
      }, 300);
    }
  }

  renderVideoOverlay() {
    const { started, live, fullscreen, viewers } = this.props;
    const {
      portraitWidth,
      portraitHeight,
      landscapeWidth,
      landscapeHeight,
      
      landscapeMarginLeft,
      landscapeMarginRight,
      paused,
      currentTime,
      duration,
      loadingVideo,
      videoError,
      castModal,
      isTv,
    } = this.state;

    if (loadingVideo) {
      // while loading the video
      return (
        <View
          style={{
            height: fullscreen ? landscapeHeight : portraitHeight,
            width: fullscreen ? landscapeWidth : portraitWidth,
            marginLeft: fullscreen ? landscapeMarginLeft : 0,
            marginRight: fullscreen ? landscapeMarginRight : 0,
            alignItems: "center",
            justifyContent: "center",
          }}
        >
          <PulseIndicator size={50} color={Colors.Indicator} />
        </View>
      );
    } else if (videoError) {
      return (
        <ImageBackground
          style={{
            width: fullscreen ? landscapeWidth : portraitWidth,
            height: fullscreen ? landscapeHeight : portraitHeight,
            alignItems: "center",
            justifyContent: "center",
          }}
          source={{ uri: this.context.activeCompany.default_video_image }}
        >
          <AppText style={{ fontSize: 16 }}>
            {global.strings.VideoError}
          </AppText>
        </ImageBackground>
      );
    } else if (started) {
      if (Platform.isTV) {
        return (
          <>
            <View
              style={{
                height: fullscreen ? landscapeHeight : portraitHeight,
                width: fullscreen ? landscapeWidth : portraitWidth,
                marginLeft: fullscreen ? landscapeMarginLeft : 0,
                marginRight: fullscreen ? landscapeMarginRight : 0,
                paddingTop: fullscreen ? 16 : 0,
              }}
            >
              <View style={{ flex: 1, flexDirection: "row" }}>
                <Touchable
                  style={{ marginLeft: 8, marginTop: 8 }}
                  onPress={() => this.props.onBack()}
                >
                  <Icon
                    name="arrow-back"
                    size={30}
                    color={
                      this.context.activeCompany.font_color || Colors.Text1
                    }
                  />
                </Touchable>
              </View>
              {/* <View style={{ flex: 1 }} /> */}
              <View
                style={{
                  flex: 1,
                  flexDirection: "row",
                  alignItems: "flex-end",
                  justifyContent: "space-between",
                }}
              >
                <View
                  style={{
                    width: "100%",
                    backgroundColor: "rgba(0,0,0,.6)",
                    flexDirection: "row",
                    height: 40,
                    alignItems: "flex-end",
                  }}
                >
                  {live == 0 && (
                    <AppText
                      time
                      style={{
                        color: "white",
                        opacity: 0.65,
                        fontSize: 14,
                        elevation: 2,
                      }}
                    >
                      {this.getConvertedCurrentTime()}
                    </AppText>
                  )}
                  <View
                    style={{
                      flex: 1,
                      flexDirection: "row",
                      justifyContent: "center",
                    }}
                  >
                    <Touchable
                      style={{ marginLeft: 8, marginTop: 8 }}
                      onPress={() => this.skipBackward()}
                    >
                      <Icon
                        name="fast-rewind"
                        size={30}
                        color={
                          this.context.activeCompany.font_color || Colors.Text1
                        }
                      />
                    </Touchable>
                    <Touchable
                      style={{ marginLeft: 8, marginTop: 8 }}
                      onPress={() => this.togglePlayer()}
                    >
                      <Icon
                        name={paused ? "play-arrow" : "pause"}
                        size={30}
                        color={
                          this.context.activeCompany.font_color || Colors.Text1
                        }
                      />
                    </Touchable>
                    <Touchable
                      style={{ marginLeft: 8, marginTop: 8 }}
                      onPress={() => this.skipForward()}
                    >
                      <Icon
                        name="fast-forward"
                        size={30}
                        color={
                          this.context.activeCompany.font_color || Colors.Text1
                        }
                      />
                    </Touchable>
                  </View>
                  {live == 0 && (
                    <AppText
                      time
                      style={{
                        opacity: 0.65,
                        fontSize: 14,
                        elevation: 2,
                        color: "white",
                      }}
                    >
                      {this.getConvertedVideoDuration()}
                    </AppText>
                  )}
                </View>
              </View>
            </View>
          </>
        );
      }
      return (
        <View>
          <Animated.View
            style={{
              height: fullscreen ? landscapeHeight : portraitHeight,
              width: fullscreen ? landscapeWidth : portraitWidth,
              marginLeft: fullscreen ? landscapeMarginLeft : 0,
              marginRight: fullscreen ? landscapeMarginRight : 0,
              paddingTop: fullscreen ? 16 : 0,
              opacity: this.overlayOpacity.interpolate({
                inputRange: [0, 1],
                outputRange: [0.0, 0.7],
              }),
            }}
          >
            <Touchable
              style={{ flex: 1 }}
              onPress={() => this.playerOpacityTimer()}
            >
              <LinearGradient
                colors={["rgba(0,0,0,.6)", "transparent"]}
                start={{ x: 0.9, y: 1.0 }}
                end={{ x: 0.9, y: 0.0 }}
                style={{
                  flex: 1,
                  alignItems: "center",
                  justifyContent: "space-between",
                }}
              >
                {/* overlay header -- LIVE tag if live and viewers counter */}
                <View
                  style={{
                    flexDirection: "row",
                    padding: 4,
                    height: 40,
                    justifyContent: live || isTv ? "space-between" : "flex-end",
                    alignSelf: "stretch",
                  }}
                >
                  {live == 1 && (
                    <View
                      style={{ flexDirection: "row", alignItems: "center" }}
                    >
                      <View
                        style={{
                          backgroundColor: "red",
                          height: 14,
                          width: 14,
                          borderRadius: 7,
                        }}
                      />
                      <AppText style={{ fontSize: 14 }}>
                        {" " + global.strings.Live}
                      </AppText>
                    </View>
                  )}
                  {isTv && (
                    <Touchable
                      style={{ marginLeft: 8, marginTop: 8 }}
                      onPress={() => this.props.onBack()}
                    >
                      <Icon
                        name="arrow-back"
                        size={30}
                        color={
                          this.context.activeCompany.font_color || Colors.Text1
                        }
                      />
                    </Touchable>
                  )}
                  {viewers != 0 && viewers != null && (
                    <View
                      style={{ flexDirection: "row", alignItems: "center" }}
                    >
                      <Icon
                        name="remove-red-eye"
                        size={14}
                        color={Colors.Text1}
                      />
                      <AppText style={{ fontSize: 14, padding: 3 }}>
                        {viewers}
                      </AppText>
                    </View>
                  )}
                </View>

                {/* overlay body -- play/pause button & seek(10) touchables on the sides */}
                <View
                  style={{
                    width: "100%",
                    flex: 1,
                    flexDirection: "column",
                  }}
                >
                  <View
                    style={{
                      flex: 1,
                      flexDirection: "row",
                      justifyContent: "flex-end",
                      paddingHorizontal: 10,
                    }}
                  >
                    {this.state.videoTextTracks.length ? (
                      <ModalSelector
                        optionTextStyle={{ padding: 1, color: "black" }}
                        selectedItemTextStyle={{
                          padding: 1,
                          backgroundColor: "black",
                          color: "white",
                        }}
                        data={this.state.pickerTextTracks}
                        scrollViewAccessibilityLabel={"Scrollable options"}
                        selectedKey={this.state.selectedTextTrackValue}
                        onChange={(option) => {
                          if (option.key === 1000) {
                            this.setState(
                              {
                                selectedTextTrackType: "disabled",
                                selectedTextTrackValue: option.key,
                              },
                              () => {}
                            );
                          } else {
                            if (typeof option.key !== "number") {
                              return false;
                            }
                            this.setState({
                              selectedTextTrackType: "index",
                              selectedTextTrackValue: option.key,
                            });
                          }
                        }}
                      >
                        <View>
                          <FontAwesomeIcon
                            icon={
                              this.state.selectedTextTrackType === "index"
                                ? faClosedCaptioning
                                : faClosedCaptioningEmpty
                            }
                            size={27}
                            color={Colors.Text1}
                          />
                        </View>
                      </ModalSelector>
                    ) : null}
                  </View>

                  <View
                    style={{
                      flex: 5,
                      flexDirection: "row",
                    }}
                  >
                    <View style={{ flex: 2 }}>
                      <Touchable
                        ref={(ref) => (this.tvButtons[1] = ref)}
                        style={{ flex: 1 }}
                        onPress={() => this.skipBackward()}
                      />
                    </View>
                    <View style={{ flex: 2 }}>
                      <Touchable
                        ref={(ref) => (this.playPauseButton = ref)}
                        onPress={() => this.togglePlayer()}
                        style={{
                          flex: 1,
                          alignItems: "center",
                          justifyContent: "center",
                        }}
                      >
                        <Icon
                          name={paused ? "play-arrow" : "pause"}
                          size={40}
                          color={Colors.Text1}
                        />
                      </Touchable>
                    </View>
                    <View style={{ flex: 2 }}>
                      <Touchable
                        ref={(ref) => (this.tvButtons[3] = ref)}
                        style={{ flex: 1 }}
                        onPress={() => this.skipForward()}
                      />
                    </View>
                  </View>
                </View>

                <View
                  style={{
                    flexDirection: "row",
                    justifyContent: "space-between",
                    alignItems: "center",
                    padding: 8,
                  }}
                >
                  {/* Overlay footer */}
                  {live == 0 && (
                    <AppText
                      time
                      style={{
                        color: "white",
                        opacity: 0.95,
                        fontSize: 14,
                        elevation: 2,
                      }}
                    >
                      {this.getConvertedCurrentTime()}
                    </AppText>
                  )}
                  {/* Video seek slider */}
                  <View style={{ flex: 1 }}>
                    {!this.state.isTv && (
                      <Slider
                        value={currentTime}
                        onValueChange={(value) => this.seekingTime(value)}
                        onSlidingStart={(value) =>
                          this.setState({ paused: true })
                        }
                        onSlidingComplete={(value) => this.seek(value)}
                        style={{ flex: 1 }}
                        minimumValue={0}
                        maximumValue={live ? currentTime : duration}
                        minimumTrackTintColor={Colors.Text1}
                        maximumTrackTintColor="grey"
                      />
                    )}
                  </View>
                  {/* Video duration label */}
                  {live == 0 && (
                    <AppText
                      time
                      style={{
                        color: "white",
                        opacity: 0.95,
                        fontSize: 14,
                        elevation: 2,
                      }}
                    >
                      {this.getConvertedVideoDuration()}
                    </AppText>
                  )}
                  {live == 1 && (
                    <AppText
                      time
                      style={{
                        opacity: 0.65,
                        fontSize: 14,
                        elevation: 2,
                      }}
                    >
                      {this.getConvertedCurrentTime()}
                    </AppText>
                  )}
                  {this.props.fullscreenController && !Platform.isTV && (
                    <Icon
                      name={fullscreen ? "fullscreen-exit" : "fullscreen"}
                      size={27}
                      color={Colors.Text1}
                      onPress={() => this.props.fullscreenController()}
                    />
                  )}
                </View>
              </LinearGradient>
            </Touchable>
          </Animated.View>
          <Modal
            visible={castModal}
            style={{
              alignItems: "center",
              justifyContent: "center",
              backgroundColor: Colors.ModalBackground,
            }}
          >
            <View
              style={{
                marginRight: 16,
                marginLeft: 16,
                backgroundColor: Colors.ModalColor,
                elevation: 1,
                borderRadius: 8,
                width: "90%",
              }}
            >
              <View style={{ alignItems: "center", justifyContent: "center" }}>
                <AppText
                  style={{ fontSize: 16, padding: 16, paddingBottom: 0 }}
                >
                  {global.strings.CastQuestion}
                </AppText>
              </View>
              <View style={{ alignItems: "center", justifyContent: "center" }}>
                <Touchable>
                  <AppText
                    style={{ fontSize: 16, padding: 8, marginBottom: 8 }}
                  >
                    {global.strings.Cast}
                  </AppText>
                </Touchable>
                <Touchable onPress={() => this.setState({ castModal: false })}>
                  <AppText
                    style={{ fontSize: 16, padding: 8, paddingBottom: 16 }}
                  >
                    {global.strings.Cancel}
                  </AppText>
                </Touchable>
              </View>
            </View>
          </Modal>
        </View>
      );
    } else {
      // view spacer in case the live is not started yet
      return <View style={{ height: portraitHeight }} />;
    }
  }

  render() {
    const { uri, fullscreen, live, started } = this.props;
    const {
      portraitWidth,
      portraitHeight,
      landscapeWidth,
      landscapeHeight,
      landscapeMarginLeft,
      landscapeMarginRight,
      paused,
      currentTime,
    } = this.state;
    return (
      <View>
        <StatusBar hidden={fullscreen} />
        {uri && started == 1 && (
          <Video
            style={{
              position: "absolute",
              width: fullscreen ? landscapeWidth : portraitWidth,
              height: fullscreen ? landscapeHeight : portraitHeight,
              marginRight: fullscreen ? landscapeMarginRight : 0,
              marginLeft: fullscreen ? landscapeMarginLeft : 0,
            }}
            textTracks={this.state.videoTextTracks}
            selectedTextTrack={{
              type: this.state.selectedTextTrackType,
              value: this.state.selectedTextTrackValue,
            }}
            paused={paused}
            ref={(ref: Video) => {
              this.video = ref;
            }}
            onLoadStart={this.onLoadStart}
            onLoad={this.onLoad}
            onProgress={this.onProgress}
            onEnd={this.onEnd}
            onError={this.onError}
            source={{
              uri:
                this.state.videoTextTracks.length > 0 ? this.props.uriMP4 : uri,
            }}
            muted={false}
            repeat={false}
            resizeMode={"contain"}
            volume={1}
            rate={1}
            ignoreSilentSwitch={"ignore"}
            // onProgressTime={thi.s}
          />
        )}

        {/* TODO: empty source -- is this a case? */}
        {uri === "" && (
          <Image
            source={{ uri: this.context.activeCompany.default_video_image }}
            style={{
              position: "absolute",
              width: fullscreen ? landscapeWidth : portraitWidth,
              height: fullscreen ? landscapeHeight : portraitHeight,
            }}
          />
        )}

        {/* case: Live not started */}
        {started == 0 && (
          <ImageBackground
            style={{
              position: "absolute",
              width: fullscreen ? landscapeWidth : portraitWidth,
              height: fullscreen ? landscapeHeight : portraitHeight,
              alignItems: "center",
              justifyContent: "center",
            }}
          >
            <View
              style={{
                flex: 1,
                flexDirection: "column",
                justifyContent: "center",
                alignItems: "center",
              }}
            >
              <AppText style={{ fontSize: 16, color: "white" }}>
                <ActivityIndicator
                  size="large"
                  color={this.context.activeCompany.font_color}
                />
              </AppText>
            </View>
          </ImageBackground>
        )}

        {/* render overlay */}
        {this.renderVideoOverlay()}
      </View>
    );
  }
}

export { VideoPlayer };
