import React from 'react';
import {View, StyleSheet, ImageBackground, FlatList} from 'react-native';

import LinearGradient from 'react-native-linear-gradient';

import {AppText, Touchable} from './common';
import {ActiveCompanyContext} from '../providers/CompanyProvider';
import Routes from '../environments/Routes';
import Colors from '../environments/Colors';
class SerieList extends React.Component {
  static contextType = ActiveCompanyContext;

  render() {
    const {data, label, serieView} = this.props;
    if (data && data.length) {
      return (
        <React.Fragment>
          <View>
            {/*<AppText style={styles.catalogLabelStyle}>{label}</AppText>*/}
            <FlatList
              showsHorizontalScrollIndicator={false}
              //horizontal
              data={data}
              renderItem={({item}) => (
                <>
                  <View style={styles.searchCatolgStyle}>
                    <ImageBackground
                      resizeMode={'contain'}
                      source={{
                        uri:
                          item.img_preview ||
                          this.context.activeCompany.default_video_image,
                      }}
                      style={styles.livePreviewStyle}
                      imageStyle={{borderRadius: 1}}
                      style={{
                        width: global.screenProp.width,
                        height: (global.screenProp.width / 16) * 9,
                        padding: 15,
                      }}>
                      <Touchable onPress={() => onItemPress(item)}>
                        {item.duration ? (
                          <AppText style={styles.durationStyle}>
                            {item.duration}
                          </AppText>
                        ) : (
                          <AppText></AppText>
                        )}
                      </Touchable>
                    </ImageBackground>
                    <AppText style={styles.liveTitleStyle}>
                      {item.title}
                    </AppText>
                    {!serieView && (
                      <AppText style={styles.liveNVideoStyle}>
                        {item.nvideo + 'video'}
                      </AppText>
                    )}
                  </View>
                </>
              )}
              keyExtractor={item => String(item.id)}
              style={styles.listStyle}
            />
          </View>
        </React.Fragment>
      );
    } else {
      return null;
    }
  }
}

const styles = StyleSheet.create({
  // list
  listStyle: {
    padding: 5,
  },
  livePreviewStyle: {
    height: 135,
    width: 240,
    borderRadius: 3,
    alignSelf: 'center',
  },
  liveDataContainerStyle: {
    marginLeft: 2,
    marginRight: 2,
    borderRadius: 3,
  },
  translucentStyle: {
    flex: 1,
    justifyContent: 'flex-end',
    borderRadius: 3,
    padding: 4,
    paddingBottom: 8,
  },
  liveButtonStyle: {
    flex: 1,
    justifyContent: 'flex-end',
    backgroundColor: 'transparent',
    borderRadius: 3,
  },
  liveTitleStyle: {
    fontWeight: 'bold',
    color: Colors.Text1,
    fontSize: 15,
    marginTop: 10,
    marginLeft: 10,
    opacity: 0.7,
  },
  liveNVideoStyle: {
    color: Colors.PlaceholderText,
    fontSize: 12,
    fontWeight: 'bold',
    marginTop: 3,
    marginLeft: 10,
  },
  catalogLabelStyle: {
    color: Colors.Text1,
    fontSize: 20,
    fontWeight: '700',
    lineHeight: 23,
    elevation: 1,
    marginLeft: 10,
    marginBottom: 4,
  },

  boxSearchSerieStyle: {
    flex: 1,
    justifyContent: 'center',
    width: '100%',
  },

  searchSerieStyle: {
    marginBottom: 30,
    justifyContent: 'center',
    width: '100%',
  },
  listStyle: {
    paddingLeft: 5,
    paddingRight: 5,
    marginBottom: 28,
  },
  livePreviewStyle: {
    width: 240,
    elevation: 1,
    alignSelf: 'center',
  },
  liveDetailsContainerStyle: {
    marginLeft: 10,
  },
  liveTitleStyle: {
    fontWeight: 'bold',
    fontSize: 15,
    opacity: 0.7,
    backgroundColor: 'black',
  },
  translucentStyle: {
    height: 135,
    width: 240,
    justifyContent: 'space-between',
    borderRadius: 1,
    padding: 4,
    paddingBottom: 8,
  },
  catalogLabelStyle: {
    fontSize: 20,
    fontWeight: '700',
    lineHeight: 23,
    elevation: 1,
    marginLeft: 10,
    marginVertical: 4,
  },
  durationStyle: {
    fontSize: 13,
    fontWeight: '700',
    padding: 4,
    marginRight: 10,
    backgroundColor: 'rgb(48,48,48)',
    alignSelf: 'flex-end',
    justifyContent: 'flex-end',
  },

  boxSearchCatolgStyle: {
    justifyContent: 'center',
  },

  searchCatolgStyle: {
    marginBottom: 30,
  },
  durationStyle: {
    fontSize: 13,
    fontWeight: '700',
    padding: 4,
    marginRight: 10,
    backgroundColor: 'rgb(48,48,48)',
    alignSelf: 'flex-end',
    justifyContent: 'flex-end',
  },
});

export {SerieList};
