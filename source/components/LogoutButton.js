import React from "react";
import { View, Animated, StyleSheet } from "react-native";

import { AppText, Touchable } from "./common";

import Colors from "../environments/Colors";

const LogoutButton = (props) => {
  const { style, onSubmit, onPress, textStyle } = props;
  const requestFeedback = new Animated.Value(0);
  // constructor() {
  //   super();
  // }

  // componentDidUpdate() {
  //   Animated.timing(
  //     this.requestFeedback, {
  //       toValue: this.props.controller ? 1 : 0,
  //       duration: 300,
  //     }).start();
  // }

  // render() {
  return (
    <Animated.View
      style={[
        styles.buttonContainer,
        {
          opacity: requestFeedback.interpolate({
            inputRange: [0, 1],
            outputRange: [1.0, 0.2],
          }),
          ...style,
        },
      ]}
    >
      <Touchable
        onPress={onSubmit}
        style={{ flex: 1, justifyContent: "center", alignItems: "center" }}
      >
        {/* this is needed to stretch the touchable field */}
        <AppText style={[styles.buttonText, textStyle]}>
          {global.strings.Logout}
        </AppText>
      </Touchable>
    </Animated.View>
  );
  // }
};

const styles = StyleSheet.create({
  buttonContainer: {
    marginTop: 25,
    borderWidth: 1,

    width: 270,
  },
  buttonText: {
    alignSelf: "center",
    fontSize: 19,
    paddingTop: 10,
    paddingBottom: 10,
  },
});

export { LogoutButton };
