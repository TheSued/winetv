import {StyleSheet, Dimensions} from 'react-native';
import Colors from '../../../environments/Colors';
let param = {
  w: Dimensions.get('screen').width,
  h: Dimensions.get('screen').height,
};
global.screenProp = {
  width: param.w < param.h ? param.w : param.h,
  height: param.w < param.h ? param.h : param.w,
};
export const slideshowStyles = StyleSheet.create({
  listStyle: {
    marginBottom: 28,
  },
  livePreviewStyle: {
    height: 230,
    width: global.screenProp.width,
    elevation: 1,
    marginHorizontal: 4,
  },
  liveTitleStyle: {
    fontWeight: 'bold',
    fontSize: 14,
    flexDirection:'row',
    textAlign: 'center',
    color:'white',
    padding:10,
    backgroundColor: 'rgb(48,48,48)',
    width: '100%',
  
  },
  imageRadius: {
    width: global.screenProp.width,
    borderRadius: 1,
  },
  translucentStyle: {
    height: 230,
    width: global.screenProp.width,
    justifyContent: 'space-between',
    borderRadius: 1,
    padding: 4,
    paddingBottom: 8,
  },
  listTitleStyle: {
    fontSize: 18,
    fontWeight: '700',
    lineHeight: 23,
    elevation: 1,
    marginLeft: 10,
    marginBottom: 4,
  },
  durationStyle: {
    fontSize: 12,
    opacity: 0.65,
    padding: 4,
    backgroundColor: 'black',
    borderRadius: 5,
    alignSelf: 'flex-end',
  },
  dotContainerStyle: {position:'relative',top:0, flexDirection: 'row', width: '100%', justifyContent:'center', paddingBottom: 10,backgroundColor:'red', marginBottom:10,},
  dotStyle: {height: 10, width: 10, marginHorizontal: 10, borderRadius: 5,},
  dotActive: {backgroundColor: 'white',},
  dotInactive: {backgroundColor: 'grey',},

  iconSliderStyle:{
    color:'red',
  }
  
});
