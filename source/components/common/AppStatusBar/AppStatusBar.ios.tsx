import React from 'react';
import {StatusBar} from 'react-native';

const AppStatusBar = (props: any) =>
  <StatusBar barStyle={'light-content'}
    hidden={props.fullscreen ? true : false}
    translucent={props.fullscreen ? false : true} />;

export default AppStatusBar;
