import React from "react";
import { View, SafeAreaView } from "react-native";
import styles from "./styles";
import AppStatusBar from "../AppStatusBar/AppStatusBar";

const BackgroundUI = (props: any) => {
  const { style, header, children, backgroundColor, fullscreen } = props;
  const { flex, paddingBottom, paddingTop } = styles;
  return (
    <>
      <AppStatusBar fullscreen={props.fullscreen} />
      <SafeAreaView
        style={[
          flex,
          fullscreen
            ? {
                justifyContent: "center",
                alignItems: "center",
              }
            : null,
          {
            backgroundColor: props.fullscreen ? "black" : backgroundColor,
          },
          fullscreen ? null : paddingTop,
          style,
        ]}
      >
        {header || null}
        <View style={[flex, paddingBottom, style]}>{children}</View>
      </SafeAreaView>
    </>
  );
};

export default BackgroundUI;
