import React from "react";
import { SafeAreaView } from "react-native";
import { KeyboardAwareScrollView } from "react-native-keyboard-aware-scroll-view";
import styles from "./styles";
import AppStatusBar from "../AppStatusBar/AppStatusBar";

const ScrollViewUI = (props: any) => {
  const { style, children, header, backgroundColor } = props;
  const { flex, paddingTop, paddingBottom } = styles;
  return (
    <>
      <AppStatusBar fullscreen={props.fullscreen} />
      <SafeAreaView
        style={[flex, paddingTop, { backgroundColor: backgroundColor }, style]}
      >
        {header || null}

        <KeyboardAwareScrollView
          showsVerticalScrollIndicator={false}
          keyboardShouldPersistTaps="always"
          style={{ flex: 1 }}
          contentContainerStyle={[{ flexGrow: 1 }, style, paddingBottom]}
        >
          {children}
        </KeyboardAwareScrollView>
      </SafeAreaView>
    </>
  );
};

export default ScrollViewUI;
