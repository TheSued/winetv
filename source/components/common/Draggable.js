import React, { Component } from 'react'
import { View, StyleSheet, PanResponder, Animated } from 'react-native'

class Draggable extends Component {
	constructor(props) {
		super(props)
		const position = new Animated.ValueXY()
		const panResponder = PanResponder.create({
			onStartShouldSetPanResponder: () => true,
			onPanResponderMove: (event, gesture) => {
				position.setValue({ x: gesture.dx, y: gesture.dy })
			},
			onPanResponderGrant: (event, gesture) => {
				gesture.dx = position.x._value
				gesture.dy = position.y._value
			},
		})
		this.state = { panResponder, position }
	}
	render() {
		const { position, panResponder } = this.state
		return (
			<Animated.View
				style={[styles.ball, position.getLayout()]}
				{...panResponder.panHandlers}
			/>
		)
	}
}
// define your styles
const styles = StyleSheet.create({
	ball: {
		height: 40,
		width: 40,
		borderColor: 'black',
		borderRadius: 20,
		borderWidth: 20
	},
})

export { Draggable }