import React, {Component} from 'react';
import {View, StyleSheet, PanResponder, Animated} from 'react-native';

import Colors from '../../environments/Colors';

class Slider extends Component {
  constructor(props) {
    super(props);
    const position = new Animated.ValueXY();
    const panResponder = PanResponder.create({
      onStartShouldSetPanResponder: () => true,
      onPanResponderMove: (event, gesture) => {
        if (gesture.dx <= this.state.maxWidth && gesture.dx > 0) {
          position.setValue({x: gesture.dx, y: 1});
          let offset = (this.props.duration / this.state.maxWidth) * gesture.dx;
          // let the player seek the location
          this.props.onMove('slider', offset);
        } else if (gesture.dx <= 0) {
          position.setValue({x: 0, y: 1});
          this.props.onMove('slider', 0);
        }
      },
      onPanResponderGrant: (event, gesture) => {
        gesture.dx = position.x._value;
        gesture.dy = position.y._value;
      },
    });
    this.state = {panResponder, position, maxWidth: 0};
  }

  shouldComponentUpdate(nextProps) {
    const {currentTime, duration} = this.props;
    const {position, maxWidth} = this.state;
    if (nextProps.currentTime != currentTime) {
      position.setValue({
        x: (maxWidth / duration) * currentTime,
        y: 1,
      });
      return true;
    } else if (nextProps.currentTime == 0) {
      position.setValue({
        x: 0,
        y: 1,
      });
      return true;
    } else if (nextProps.currentTime == duration) {
      position.setValue({
        x: maxWidth,
        y: 1,
      });
      return true;
    } else {
      return false;
    }
  }

  render() {
    const {position, panResponder, maxWidth} = this.state;
    return (
      <View
        style={{alignItems: 'center', flexDirection: 'row', height: 20, paddingHorizontal: 8}}
        onLayout={(event) => this.setState({maxWidth: event.nativeEvent.layout.width - 16})}>
        <Animated.View
          style={{
            flex: 1,
            borderRadius: 5,
            height: 10,
            borderLeftColor: Colors.Secondary1,
            borderLeftWidth: position.x._value,
            borderRightWidth: maxWidth - position.x._value,
            borderRightColor: 'grey',
          }}
        />
        <Animated.View style={[styles.ball, position.getLayout()]} {...panResponder.panHandlers} />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  ball: {
    backgroundColor: 'blue',
    position: 'absolute',
    height: 20,
    width: 20,
    borderRadius: 10,
  },
});
export {Slider};
