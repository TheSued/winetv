import React from "react";
import { View, ImageBackground, FlatList, Platform } from "react-native";

import LinearGradient from "react-native-linear-gradient";

import { AppText, Touchable } from "../common";
import { homepageStyles } from "./HomepageListStyle";
import { ActiveCompanyContext } from "../../providers/CompanyProvider";
import Icon from "react-native-vector-icons/MaterialCommunityIcons";

const HomePageList = (props: any) => {
  const {
    data,
    id,
    title,
    onItemPress,
    onEndReached,
    onEndReachedThreshold,
    defaultPreview,
    liveTitleStyle,
    listStyle,
    itemStyle,
    imageSize,
    id_cat,
    onTitlePress,
    ...otherProps
  } = props;
  if (data && data.length) {
    return (
      <View
        style={
          Platform.isTV
            ? {
                width: "100%",
                alignItems: "center",
                elevation: 0,
              }
            : listStyle
        }
      >
        {!!title && (
          <Touchable onPress={onTitlePress} style={homepageStyles.titlePress}>
            <AppText style={homepageStyles.listTitleStyle}>{title}</AppText>
              {id !== "live_scheduled" &&
              id !== "live_now" &&
              id !== "keep_watching" &&
              id !== "" &&
              id_cat !== "" && (
                <AppText style={homepageStyles.sfogliaStyle}>
                  {global.strings.showAll}
                  <Icon
                    style={homepageStyles.iconStyle}
                    name="chevron-right-circle-outline"
                    size={12}
                  />
                </AppText>
              )}
          </Touchable>
        )}

        {/* list */}
        <FlatList
          keyExtractor={(item) => "" + item.id}
          style={homepageStyles.listStyle}
          horizontal={props.horizontal}
          onEndReachedThreshold={10}
          onEndReached={onEndReached}
          {...otherProps}
          showsHorizontalScrollIndicator={false}
          data={data}
          renderItem={({ item }) => (
            <View style={[itemStyle]}>
              <Touchable onPress={() => onItemPress(item)}>
                <ImageBackground
                  resizeMode={"cover"}
                  source={{ uri: item.img_preview || defaultPreview }}
                  style={[homepageStyles.livePreviewStyle, imageSize]}
                  imageStyle={homepageStyles.imageRadius}
                >
                  {/*<LinearGradient
                    colors={['black', 'transparent']}
                    start={{ x: 0.2, y: 1.0 }}
                    end={{ x: 0.2, y: 0.0 }}
                    style={homepageStyles.translucentStyle}
                  > </LinearGradient> 
                    style={[slideshowStyles.dotStyle, this.index === dotIndex ? slideshowStyles.dotActive : slideshowStyles.dotInactive]} */}

                  <View style={homepageStyles.lockStyle}>
                    {item.isCourse === 1 && item.isEnabled === 0 && (
                      <Icon name="lock" size={30} color={"white"} />
                    )}

                    {item.duration ? (
                      <AppText style={homepageStyles.durationStyle}>
                        {item.duration}
                      </AppText>
                    ) : (
                      <AppText />
                    )}
                  </View>
                </ImageBackground>
              </Touchable>

              <AppText style={homepageStyles.liveTitleStyle}>
                {item.title.substring(0, 60)}
              </AppText>
            </View>
          )}
        />
      </View>
    );
  } else {
    return null;
  }
};

export { HomePageList };
