import React from 'react'
import { StyleSheet, View, Platform } from 'react-native'

import { AppText } from '../components/common'

class Toast extends React.Component {

	render() {
		if (this.props.message != '') {
			return (
				<View style={styles.superView} >
					<View style={styles.toast} >
						<AppText style={styles.message} >{this.props.message}</AppText>
					</View>
				</View>
			)
		} else {
			return null
		}

	}
}

const styles = StyleSheet.create({
	superView: {
		position: 'absolute',
		bottom: 0,
		left: 0,
		right: 0,
		alignItems: 'center',
		justifyContent: 'center',
		height: 70,
	},
	toast: {
		backgroundColor: 'white',
		paddingHorizontal: 12,
		paddingVertical: 6,
		elevation: 2,
		borderRadius: 5,
		marginBottom: Platform.OS == 'ios' ? 60 : 85,
		marginLeft: 16,
		marginRight: 16,
	},
	message: {
		color: 'black',
		fontSize: 16,
	}
})

export { Toast }