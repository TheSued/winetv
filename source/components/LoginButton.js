import React from "react";
import { View, Animated, StyleSheet } from "react-native";

import { AppText, Touchable } from "./common";

import Colors from "../environments/Colors";

class LoginButton extends React.Component {
  constructor() {
    super();
    this.requestFeedback = new Animated.Value(0);
  }

  componentDidUpdate() {
    Animated.timing(this.requestFeedback, {
      toValue: this.props.controller ? 1 : 0,
      duration: 400,
      useNativeDriver: false,
    }).start();
  }

  render() {
    return (
      <Animated.View
        style={[
          styles.buttonContainer,
          {
            opacity: this.requestFeedback.interpolate({
              inputRange: [0, 1],
              outputRange: [1.0, 0.2],
            }),
            ...this.props.style,
          },
        ]}
      >
        <Touchable
          onPress={this.props.onSubmit}
          style={{ flex: 1, justifyContent: "center", alignItems: "center" }}
        >
          <AppText style={[styles.buttonText, { color: this.props.color }]}>
            {global.strings.SignIn}
          </AppText>
        </Touchable>
      </Animated.View>
    );
  }
}

const styles = StyleSheet.create({
  buttonContainer: {
    marginTop: 25,
    borderWidth: 1,
    borderStyle: "solid",
    elevation: 1,
    width: 300,
    backgroundColor: "transparent",
  },
  buttonText: {
    alignSelf: "center",
    fontSize: 22,
    paddingTop: 12,
    paddingBottom: 12,
    elevation: 1,
  },
});

export { LoginButton };
