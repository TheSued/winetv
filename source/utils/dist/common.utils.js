"use strict";
var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
exports.__esModule = true;
exports.formatCustomCartObject = exports.formatVideoObject = void 0;
function formatVideoObject(videoInfo) {
    if (videoInfo) {
        var video = {};
        if (videoInfo.type == "ondemand") {
            video = {
                live: 0,
                started: 1
            };
            if (videoInfo.source_fullhd) {
                video = __assign(__assign({}, video), { fullhd: videoInfo.source_fullhd });
            }
            if (videoInfo.source_hd) {
                video = __assign(__assign({}, video), { hd: videoInfo.source_hd });
            }
            if (videoInfo.source_hq) {
                video = __assign(__assign({}, video), { hq: videoInfo.source_hq });
            }
            if (videoInfo.source_original) {
                video = __assign(__assign({}, video), { original: videoInfo.source_original });
            }
        }
        else {
            video = {
                live: 1,
                started: 0
            };
            if (videoInfo.source_hls) {
                video = __assign(__assign({}, video), { hls: videoInfo.source_hls });
            }
            else {
                video = __assign(__assign({}, video), { hls: "" });
            }
        }
        return video;
    }
    else {
        console.log("App tried to format a video object with incorrect data: ", videoInfo);
        return 0;
    }
}
exports.formatVideoObject = formatVideoObject;
function formatCustomCartObject(cartInfo) {
    if (cartInfo) {
        var cartData_1 = {};
        cartInfo.forEach(function (data) {
            if (data.val.id == "country") {
                if (data) {
                    cartData_1 = __assign(__assign({}, cartData_1), { countryCheckPrice: data.val.checkprice, countryRequired: data.required });
                }
            }
            if (data.val.id == "fullname") {
                if (data) {
                    cartData_1 = __assign(__assign({}, cartData_1), { fullnameCheckPrice: data.val.checkprice, fullnameRequired: data.required });
                }
            }
            if (data.val.id == "fiscal_code") {
                if (data) {
                    cartData_1 = __assign(__assign({}, cartData_1), { taxCheckPrice: data.val.checkprice, taxRequired: data.required });
                }
            }
            if (data.val.id == "checkbox-iscompany") {
                if (data) {
                    cartData_1 = __assign(__assign({}, cartData_1), { checkboxCheckPrice: data.val.checkprice, checkboxValue: data.val.value, checkboxChecked: data.val.checked, checkboxLabel: data.val.label, checkboxRequired: data.required, checkboxControlShowInput: data.control_show_input, checkboxControl: data.control });
                }
            }
            if (data.val.id == "city") {
                if (data) {
                    cartData_1 = __assign(__assign({}, cartData_1), { cityCheckPrice: data.val.checkprice, cityRequired: data.required });
                }
            }
            if (data.val.id == "province") {
                if (data) {
                    cartData_1 = __assign(__assign({}, cartData_1), { provinceCheckPrice: data.val.checkprice, provinceRequired: data.required });
                }
            }
            if (data.val.id == "cap") {
                if (data) {
                    cartData_1 = __assign(__assign({}, cartData_1), { capCheckPrice: data.val.checkprice, capRequired: data.required });
                }
            }
            if (data.val.id == "sdi_code_pec") {
                if (data) {
                    cartData_1 = __assign(__assign({}, cartData_1), { sdicodepecCheckPrice: data.val.checkprice, sdicodepecRequired: data.required });
                }
            }
            if (data.val.id == "vat_num") {
                if (data) {
                    cartData_1 = __assign(__assign({}, cartData_1), { vatnumCheckPrice: data.val.checkprice, vatnumPlaceholder: data.val.placeholder, vatnumValidation: data.validation_vat_num, vatnumRequired: data.required });
                }
            }
            if (data.val.id == "phone") {
                if (data) {
                    cartData_1 = __assign(__assign({}, cartData_1), { phoneCheckPrice: data.val.checkprice, phoneRequired: data.required });
                }
            }
            if (data.val.id == "email_for_invoice") {
                if (data) {
                    cartData_1 = __assign(__assign({}, cartData_1), { emailInvoiceCheckPrice: data.val.checkprice, emailInvoiceRequired: data.required });
                }
            }
        });
        return cartData_1;
    }
    else {
        console.log("App tried to format a video object with incorrect data: ", cartInfo);
        return 0;
    }
}
exports.formatCustomCartObject = formatCustomCartObject;
