"use strict";
exports.__esModule = true;
var react_native_google_cast_1 = require("react-native-google-cast");
var castState = function () {
    var client = react_native_google_cast_1.useRemoteMediaClient();
    if (client) {
        return true;
    }
    else {
        return false;
    }
};
exports["default"] = castState;
