export const formatVideoList = (bundleArray) => {
    const resultArray = [];
    bundleArray.forEach((data) => {
        const result = {};
        result.avatar = data.avatar;
        result.collection = data.collection;
        result.collection_title = data.collection_title;
        result.collection_visible = data.collection_visible;
        result.date = data.date;
        result.description = data.description;
        result.display_name = data.display_name;
        result.duration = data.duration;
        result.img_preview = data.img_preview;
        result.livenow = data.livenow;
        result.markers = data.markers;
        result.related_product = data.related_product;
        result.title = data.title;
        resultArray.push(result);
    });

    return resultArray;
};

export const formatVideoRelatedPreview = (bundleArray) => {
    const resultArray = [];
    resultArray.push(bundleArray);

    return resultArray;
};
