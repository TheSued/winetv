export const formatSerie = (serieArray) => {
  const resultArray = [];
  serieArray.forEach((serie) => {
    const result = {};

    result.id = serie.id ? serie.id : serie.id_parent;
    result.access = serie.access;
    result.parent = serie.parent;
    result.child = serie.child;
    result.creator = serie.creator;
    result.display_name = serie.display_name;
    result.title = serie.title;
    result.description = serie.description;
    result.img_preview = serie.img_preview;
    result.trailer = serie.trailer;
    result.nvideo = serie.nvideo;
    result.app_products = serie.app_products;
    result.payment = {
      subscription: [],
    };

    if (serie.price) {
      result.payment.price = serie.price;
    }
    if (serie.sub_price_month) {
      result.payment.subscription.push({
        period: 1,
        price: serie.sub_price_month,
      });
    }
    if (serie.sub_price_quarterly) {
      result.payment.subscription.push({
        period: 3,
        price: serie.sub_price_quarterly,
      });
    }
    if (serie.sub_price_half_yearly) {
      result.payment.subscription.push({
        period: 6,
        price: serie.sub_price_half_yearly,
      });
    }
    if (serie.sub_price_year) {
      result.payment.subscription.push({
        period: 12,
        price: serie.sub_price_year,
      });
    }
    if (serie.days_trial) {
      result.days_trial = serie.days_trial;
    }
    resultArray.push(result);
  });
  return resultArray;
};

export const formatBundle = (bundleArray) => {
  const resultArray = [];
  bundleArray.forEach((serie) => {
    const result = {};
    result.id = +serie.id_child;
    result.creator = serie.creator;
    result.display_name = serie.display_name;
    result.title = serie.title;
    result.img_preview = serie.img_preview;
    resultArray.push(serie);
  });
  return resultArray;
};

export const formatCustCart = (bundleArray) => {
  const resultArray = [];
  bundleArray.forEach((data) => {
    const result = {};
    result.type = data.type;
    result.val = data.val;
    result.control = data.control;
    result.control_show_input = data.control_show_input;
    result.validation_vat_num = data.validation_vat_num;
    result.required = data.required;
    resultArray.push(result);
  });

  return resultArray;
};

export const formatCustCartInput = (bundleArray) => {
  const resultArray = [];
  bundleArray.input_feedback.forEach((data) => {
    const result = {};
    result.input = data.input;
    result.msg = data.msg;
    resultArray.push(result);
  });
  const data = {
    input: "",
    msg: "",
    tax_rate_name: bundleArray.tax_rate_name,
    tax_rate_valid: bundleArray.tax_rate_valid,
    tax_rate_value: bundleArray.tax_rate_value,
    tax_rate_amount_value: bundleArray.tax_rate_amount_value,
  };
  resultArray.push(data);

  // resultArray.push(bundleArray.tax_rate_name);
  // resultArray.push(bundleArray.tax_rate_valid);
  // resultArray.push(bundleArray.tax_rate_value);
  // resultArray.push(bundleArray.tax_rate_amount_value);

  return resultArray;
};

// export const hideAndSeekWithResponse = (response) => {
//   if (response.length === 1) {
//     return response[0];
//   } else if (response.status) {
//     return response;
//   }
// };

// access: "not_paid"
// child: true
// creator: "creator-demo"
// description: "SerieBundle1"
// display_name: "seriebundle1"
// id: 374
// days_trial: 3
// img_preview: ""
// title: "SerieBundle1"
// parent: false
// trailer: null
// sub_price_half_yearly: null
// sub_price_month: null
// sub_price_quarterly: null
// sub_price_year: 5
// price: 10
// nvideo: 0

// certificate: 0
// company_display_name: "creator-demo"
// creators: [{…}]
// home_serie: null
// idc: "199"
// mature: "0"
// nVideoLiveCarried: 3
// nVideoLiveScheduled: 0
// nVideoOndemand: 0
// nVideoQuiz: 0
// nVotes: 0
// nlive: 0
// original: null
// payment_type: "fixed_and_subs"
// rating: 0

// subtitles: []
// views: null
