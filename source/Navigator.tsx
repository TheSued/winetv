import React from "react";
import { SafeAreaProvider } from "react-native-safe-area-context";
import { NavigationContainer } from "@react-navigation/native";
import { createStackNavigator } from "@react-navigation/stack";
import * as Screen from "./views";
import { ActiveCompanyContext } from "./providers";
import { Splashscreen } from "./views";
import { Client } from "./views";

const Stack = createStackNavigator();

export default function Navigator() {
  return (
    <ActiveCompanyContext.Consumer>
      {(context) =>
        context.isLoading ? (
          <Splashscreen />
        ) : (
          <SafeAreaProvider>
            <NavigationContainer>
              <Stack.Navigator headerMode="none">
                {context.activeCompany.home_guest ? (
                  <>
                    <Stack.Screen name="Homepage" component={Screen.Homepage} />
                    <Stack.Screen
                      name="Searchpage"
                      component={Screen.Searchpage}
                    />
                    <Stack.Screen name="Profile" component={Screen.Profile} />
                    <Stack.Screen
                      name="ManageLive"
                      component={Screen.ManageLive}
                    />
                    <Stack.Screen name="AddLive" component={Screen.AddLive} />
                    <Stack.Screen name="Player" component={Screen.Player} />
                    <Stack.Screen
                      name="StreamView"
                      component={Screen.StreamView}
                    />
                    <Stack.Screen
                      name="DebuggerView"
                      component={Screen.DebuggerView}
                    />
                    <Stack.Screen
                      name="SerieView"
                      component={Screen.SerieView}
                    />

                    <Stack.Screen name="Login" component={Screen.LoginForm} />
                    <Stack.Screen
                      name="Registration"
                      component={Screen.RegistrationForm}
                    />
                    <Stack.Screen
                      name="WebViewPage"
                      component={Screen.WebViewPage}
                    />
                  </>
                ) : !context.user ? (
                  <>
                    {/* public screens */}
                    <Stack.Screen name="Login" component={Screen.LoginForm} />
                    <Stack.Screen
                      name="Registration"
                      component={Screen.RegistrationForm}
                    />
                    <Stack.Screen
                      name="WebViewPage"
                      component={Screen.WebViewPage}
                    />
                  </>
                ) : (
                  <>
                    {/* protected screens */}
                    <Stack.Screen name="Homepage" component={Screen.Homepage} />
                    <Stack.Screen
                      name="Searchpage"
                      component={Screen.Searchpage}
                    />
                    <Stack.Screen name="Profile" component={Screen.Profile} />
                    <Stack.Screen
                      name="ManageLive"
                      component={Screen.ManageLive}
                    />
                    <Stack.Screen name="AddLive" component={Screen.AddLive} />
                    <Stack.Screen name="Player" component={Screen.Player} />
                    <Stack.Screen
                      name="StreamView"
                      component={Screen.StreamView}
                    />
                    <Stack.Screen
                      name="DebuggerView"
                      component={Screen.DebuggerView}
                    />
                    <Stack.Screen
                      name="SerieView"
                      component={Screen.SerieView}
                    />
                    {/* <Stack.Group screenOptions={{ presentation: "modal" }}>
                      <Stack.Screen
                        name="ModalPay"
                        component={Screen.InAppView}
                      />
                    </Stack.Group> */}

                    <Stack.Screen
                      name="WebViewPage"
                      component={Screen.WebViewPage}
                    />

                    <Stack.Screen name="CastPage" component={Screen.Client} />
                  </>
                )}
              </Stack.Navigator>
            </NavigationContainer>
          </SafeAreaProvider>
        )
      }
    </ActiveCompanyContext.Consumer>
  );
}
