import React from 'react';
import {FlatList, View, TouchableWithoutFeedback} from 'react-native';

import {AppText} from '../components/common';
import {ActiveCompanyContext} from '../providers/CompanyProvider';

class DebuggerView extends React.PureComponent {
  static contextType = ActiveCompanyContext;

  state = {
    debugLog: [],
    pageHasFocus: false,
  };

  componentDidMount() {
    this.focusListener = this.props.navigation.addListener('focus', () =>
      this.setState({debugLog: this.context.debugLog, pageHasFocus: true}),
    );
    this.focusListener = this.props.navigation.addListener('blur', () =>
      this.setState({pageHasFocus: false}),
    );
  }

  componentWillUnmount() {
    this.focusListener();
  }

  render() {
    if (this.state.pageHasFocus) {
      return (
        <>
          <TouchableWithoutFeedback
            onPress={() => this.props.navigation.navigate('Profile')}>
            <View
              style={{
                alignItems: 'center',
                justifyContent: 'center',
              }}>
              <AppText style={{fontSize: 16, padding: 8, paddingBottom: 16}}>
                {'Debugger'}
              </AppText>
            </View>
          </TouchableWithoutFeedback>
          <FlatList
            style={{flex: 1, padding: 3}}
            data={this.state.debugLog || []}
            renderItem={({item}: any) => (
              <AppText>
                {'###########\n' +
                  JSON.stringify(item.code) +
                  ':\n' +
                  item.message +
                  '\n' +
                  +'###########'}
              </AppText>
            )}
            keyExtractor={(item: any) => item.id}
          />
        </>
      );
    } else {
      return <Background empty />;
    }
  }
}

export {DebuggerView};
