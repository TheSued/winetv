import React, { useEffect, useContext, useState } from "react";
import {
  StyleSheet,
  Button,
  SafeAreaView,
  Text,
  View,
  Pressable,
  Image,
} from "react-native";
import {
  CastButton,
  MediaPlayerState,
  useMediaStatus,
  useRemoteMediaClient,
  useStreamPosition,
  MediaTrack,
  CastContext,
} from "react-native-google-cast";

import { ActiveCompanyContext } from "../providers/CompanyProvider";
import { FontAwesomeIcon } from "@fortawesome/react-native-fontawesome";
import {
  faPlayCircle,
  faPlay,
  faPause,
  faForward,
  faBackward,
  faVolumeMute,
  faVolumeUp,
} from "@fortawesome/free-solid-svg-icons";
import { faClosedCaptioning } from "@fortawesome/free-solid-svg-icons";
import { faClosedCaptioning as faClosedCaptioningEmpty } from "@fortawesome/free-regular-svg-icons";
import ModalSelector from "react-native-modal-selector";
import * as Network from "../network";
import Orientation from "react-native-orientation-locker";

function Client(props: any) {
  const [subtitles, setSubtitles] = useState<any>([]);
  const [subtitleOptions, setSubtitleOptions] = useState<any>([]);
  const [selectedTextTrackValue, setSelectedTextTrackValue] =
    useState<any>(1000);
  const client = useRemoteMediaClient();
  const status = useMediaStatus();
  const streamPosition = useStreamPosition();
  const activeCompany: any = useContext(ActiveCompanyContext);
  const { video } = props.route.params;
  const { videoId } = props.route.params;

  useEffect(() => {
    const started = client?.onMediaPlaybackStarted(() =>
      console.log("playback started")
    );
    const ended = client?.onMediaPlaybackEnded(() =>
      console.log("playback ended")
    );

    return () => {
      started?.remove();
      ended?.remove();
    };
  }, [client]);

  const getSubtitles = async (id: string) => {
    let subtitles = await Network.getVideoSubtitles(id);

    let newSubtitles: any = [];
    let newData: any = [];
    subtitles.map((subtitle: any, index: number) => {
      newSubtitles.push({
        contentId: subtitle.subtitles,
        id: index,
        type: "text",
        subtype: "subtitles",
        name: subtitle.language + " Subtitle",
        language: subtitle.language,
      });
      newData.push({
        key: index,
        label: subtitle.language,
      });
    });
    setSubtitles([...newSubtitles]);
    setSubtitleOptions([
      {
        key: 1000,
        label: "NONE",
      },
      ...newData,
    ]);
  };
  useEffect(() => {
    const unsubscribe = props.navigation.addListener("focus", () => {
      Orientation.lockToPortrait();
    });
    const subscription = CastContext.onCastStateChanged((castState) => {
      if (castState === "notConnected" || castState === "noDevicesAvailable") {
        Orientation.unlockAllOrientations();
        props.navigation.goBack();
      }
    });
    getSubtitles(videoId);
    return () => {
      unsubscribe;
      Orientation.unlockAllOrientations();
      subscription.remove();
    };
  }, []);
  if (!client) {
    return (
      <SafeAreaView
        style={{
          flex: 1,
          backgroundColor: activeCompany.backgroundColor,
        }}
      >
        <View
          style={{
            flex: 1,
            justifyContent: "center",
            alignItems: "center",
            flexDirection: "column",
          }}
        >
          <Text style={{ margin: 10, color: activeCompany.fontColor }}>
            Connect to a Cast device to establish a session
          </Text>
        </View>
      </SafeAreaView>
    );
  }

  return (
    <SafeAreaView
      style={{
        backgroundColor: activeCompany.backgroundColor,
        flex: 1,
      }}
    >
      <View
        style={{
          flex: 1,

          flexDirection: "column",
        }}
      >
        <View
          style={{
            flex: 2,
            flexDirection: "row",
            justifyContent: "center",
            alignItems: "center",
          }}
        >
          {/* <Pressable
            onPress={() => {
              sessionManager.endCurrentSession(true);
            }}
            style={({ pressed }) => [
              {
                borderRadius: 2000,
                backgroundColor: pressed
                  ? activeCompany.buttonColor
                  : activeCompany.backgroundColor,
              },
            ]}
          >
            <FontAwesomeIcon
              icon={faPowerOff}
              size={40}
              color={activeCompany.fontColor}
            />
          </Pressable> */}

          <CastButton
            style={{
              backgroundColor: activeCompany.buttonColor,
              borderRadius: 10,

              height: 65,
              width: 65,
              tintColor: activeCompany.buttonTextColor,
            }}
          />
        </View>
        <View
          style={{
            flex: 3,
            flexDirection: "row",
            alignItems: "center",
            justifyContent: "center",
          }}
        >
          <Image
            style={styles.thumbnail}
            source={{ uri: activeCompany.actualPreviewUri }}
          />
        </View>
        <View
          style={{
            flex: 2,
            flexDirection: "column",
            alignItems: "center",
            justifyContent: "center",
          }}
        >
          {!status || status.playerState === MediaPlayerState.IDLE ? (
            <View>
              <Pressable
                onPress={() => {
                  client.loadMedia({
                    mediaInfo: {
                      contentUrl: video,
                      contentType: "application/x-mpegURL",
                      mediaTracks: subtitles,
                    },
                  });
                }}
                style={({ pressed }) => [
                  {
                    borderRadius: 2000,
                    backgroundColor: pressed
                      ? activeCompany.buttonColor
                      : activeCompany.backgroundColor,
                  },
                ]}
              >
                <FontAwesomeIcon
                  icon={faPlayCircle}
                  size={60}
                  color={activeCompany.fontColor}
                />
              </Pressable>
            </View>
          ) : (
            <View
              style={{
                flexDirection: "row",
                justifyContent: "space-between",
                alignItems: "center",
              }}
            >
              <View style={{ marginHorizontal: 15 }}>
                <Pressable
                  onPress={() => client.seek({ position: -10, relative: true })}
                  style={({ pressed }) => [
                    {
                      borderRadius: 2000,
                      backgroundColor: pressed
                        ? activeCompany.buttonColor
                        : activeCompany.backgroundColor,
                    },
                  ]}
                >
                  <FontAwesomeIcon
                    icon={faBackward}
                    size={40}
                    color={activeCompany.fontColor}
                  />
                </Pressable>
              </View>
              <View style={{ marginHorizontal: 15 }}>
                {status.playerState === MediaPlayerState.PAUSED ? (
                  <Pressable
                    onPress={() => {
                      client.play();
                    }}
                    style={({ pressed }) => [
                      {
                        borderRadius: 2000,
                        backgroundColor: pressed
                          ? activeCompany.buttonColor
                          : activeCompany.backgroundColor,
                      },
                    ]}
                  >
                    <FontAwesomeIcon
                      icon={faPlay}
                      size={40}
                      color={activeCompany.fontColor}
                    />
                  </Pressable>
                ) : (
                  <Pressable
                    onPress={() => {
                      client.pause();
                    }}
                    style={({ pressed }) => [
                      {
                        borderRadius: 2000,
                        backgroundColor: pressed
                          ? activeCompany.buttonColor
                          : activeCompany.backgroundColor,
                      },
                    ]}
                  >
                    <FontAwesomeIcon
                      icon={faPause}
                      size={40}
                      color={activeCompany.fontColor}
                    />
                  </Pressable>
                )}
              </View>
              <View style={{ marginHorizontal: 15 }}>
                <Pressable
                  onPress={() => client.seek({ position: 10, relative: true })}
                  style={({ pressed }) => [
                    {
                      borderRadius: 2000,
                      backgroundColor: pressed
                        ? activeCompany.buttonColor
                        : activeCompany.backgroundColor,
                    },
                  ]}
                >
                  <FontAwesomeIcon
                    icon={faForward}
                    size={40}
                    color={activeCompany.fontColor}
                  />
                </Pressable>
              </View>
              <View style={{ marginHorizontal: 15 }}>
                <Pressable
                  onPress={() => client.setStreamMuted(!status.isMuted)}
                  style={({ pressed }) => [
                    {
                      borderRadius: 2000,
                      backgroundColor: pressed
                        ? activeCompany.buttonColor
                        : activeCompany.backgroundColor,
                    },
                  ]}
                >
                  <FontAwesomeIcon
                    icon={status.isMuted ? faVolumeUp : faVolumeMute}
                    size={40}
                    color={activeCompany.fontColor}
                  />
                </Pressable>
              </View>
              <View>
                {subtitles.length ? (
                  <ModalSelector
                    optionTextStyle={{ padding: 1, color: "black" }}
                    selectedItemTextStyle={{
                      padding: 1,
                      backgroundColor: "black",
                      color: "white",
                    }}
                    data={subtitleOptions}
                    scrollViewAccessibilityLabel={"Scrollable options"}
                    selectedKey={selectedTextTrackValue}
                    onChange={(option: any) => {
                      option.key !== 1000
                        ? () => {
                            client.setActiveTrackIds([option.key]);
                            setSelectedTextTrackValue(option.key);
                          }
                        : () => {
                            client.setActiveTrackIds([]);
                            setSelectedTextTrackValue(1000);
                          };
                    }}
                  >
                    <View>
                      <FontAwesomeIcon
                        icon={
                          selectedTextTrackValue !== 1000
                            ? faClosedCaptioning
                            : faClosedCaptioningEmpty
                        }
                        size={27}
                        color={activeCompany.fontColor}
                      />
                    </View>
                  </ModalSelector>
                ) : null}
              </View>
            </View>
          )}
        </View>
      </View>
    </SafeAreaView>
  );
}
const styles = StyleSheet.create({
  thumbnail: {
    borderRadius: 5,
    width: 200,
    height: (200 * 9) / 16,
  },
});

export { Client };
