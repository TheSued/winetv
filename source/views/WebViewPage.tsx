import React from "react";
import { storage } from "react-native-firebase";
import { WebView } from "react-native-webview";
import { Background, Touchable, AppText } from "../components/common";
import Storage from "../services/Storage";
import { StyleSheet, View, ActivityIndicator, Dimensions } from "react-native";
import { ActiveCompanyContext } from "../providers/CompanyProvider";
import Icon from "react-native-vector-icons/MaterialIcons";

class WebViewPage extends React.PureComponent {
  static contextType = ActiveCompanyContext;
  state = {
    pageHasFocus: false,
    paramsReady: false,
    visible: true,
  };
  webview = null;
  webViewParams: any;
  currentNavigation: any;
  handleClick({}) {}

  componentDidMount() {
    this.getWebViewAuth();
    this.focusListener = this.props.navigation.addListener("focus", () =>
      this.setState({ pageHasFocus: true })
    );
    this.blurListener = this.props.navigation.addListener("blur", () =>
      this.setState({ pageHasFocus: false })
    );
  }
  hideSpinner() {
    this.setState({ visible: false });
  }
  async getWebViewAuth() {
    const token = await Storage.getToken();
    this.webViewParams = { uri: this.props.route.params.source };
    if (token) {
      this.webViewParams.headers = { Authorization: token };
    }

    this.setState({ paramsReady: true });
  }
  windowWidth = Dimensions.get("window").width;
  windowHeight = Dimensions.get("window").height;
  render() {
    if (this.state.pageHasFocus && this.state.paramsReady) {
      return (
        <View style={{ flex: 1 }}>
          <WebView
            onLoad={() => this.hideSpinner()}
            source={this.webViewParams}
          />
          {this.state.visible && (
            <ActivityIndicator
              style={{
                position: "absolute",
                top: this.windowHeight / 2,
                left: this.windowWidth / 2,
                right: this.windowWidth / 2,
                bottom: this.windowHeight / 2,
              }}
              size="large"
              color={this.context.buttonColor}
            />
          )}
          <View style={styles.closeButton}>
            <Touchable onPress={() => this.props.navigation.goBack()}>
              <Icon
                backgroundColor="#3b5998"
                name="arrow-back"
                size={40}
                color="#000"
              />
            </Touchable>
          </View>
        </View>
      );
    } else {
      return <Background empty />;
    }
  }
}
const styles = StyleSheet.create({
  closeButton: {
    padding: 2,
    borderRadius: 200,
    backgroundColor: "white",
    position: "absolute",
    top: 40,
    left: 5,
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 11,
    },
    shadowOpacity: 0.55,
    shadowRadius: 14.78,

    elevation: 22,
  },
});
export { WebViewPage };
