import React from "react";
import { View, Image, StyleSheet, Platform, NativeModules } from "react-native";
import { LogoutButton, Modal } from "../components";
import Colors from "../environments/Colors";
import Storage from "../services/Storage";

import { AppCompanyId, isCustomApp } from "../environments/AppEnvironment";
import * as Network from "../network";
import { ActiveCompanyContext } from "../providers";
import AppStatusBar from "../components/common/AppStatusBar/AppStatusBar";
import {
  OrientationLocker,
  PORTRAIT,
  LANDSCAPE,
} from "react-native-orientation-locker";
import { AppText, Touchable } from "../components/common";

class Splashscreen extends React.PureComponent {
  static contextType = ActiveCompanyContext;
  state = {
    verifyMailStep: false,
  };

  componentDidMount() {
    this.initializeApp();
  }

  async initializeApp() {
    await this.setCompany();
    const token = await Storage.getToken();
    try {
      if (token) {
        await this.initializeUser();
        return;
      } else {
        this.context.signedIn(null);
      }
    } catch (error) {
      console.log(error);
    }
  }

  async initializeUser() {
    let lanSuffix = "";
    const locale = this.getLocale();
    if (locale.includes("it_")) {
      lanSuffix = "it";
      this.context.setCompanySettingsLanguage(lanSuffix);
    } else if (locale.includes("en_")) {
      lanSuffix = "en";
      this.context.setCompanySettingsLanguage(lanSuffix);
    } else if (locale.includes("es_")) {
      lanSuffix = "es";
      this.context.setCompanySettingsLanguage(lanSuffix);
    } else if (locale.includes("fr_")) {
      lanSuffix = "fr";
      this.context.setCompanySettingsLanguage(lanSuffix);
    }
    const data = await this.getData();

    if (data) {
      const userData = this.formatUser(data);
      const companySettings = await Network.getCompanySettings(
        data.companies[0].id,
        Platform.OS,
        this.context.languageCompanySettings
      );
      const companyData = this.formatCompany(data, companySettings);
      if (!data.email_verified) {
        this.setState({ verifyMailStep: true });
        return;
      }

      this.context.signedIn(userData);
      return;
    } else {
      this.context.signedIn(null);
    }
  }

  getLocale(): string {
    switch (Platform.OS) {
      case "ios":
        return (
          (NativeModules.SettingsManager.settings.AppleLocale &&
            NativeModules.SettingsManager.settings.AppleLocale.replace(
              "-",
              "_"
            )) ||
          NativeModules.SettingsManager.settings.AppleLanguages[0]
        );
      case "android":
        return NativeModules.I18nManager.localeIdentifier;
    }
    return "";
  }

  async setCompany() {
    let lanSuffix = "";
    const locale = this.getLocale();
    if (locale.includes("it_")) {
      lanSuffix = "it";
      this.context.setCompanySettingsLanguage(lanSuffix);
    } else if (locale.includes("en_")) {
      lanSuffix = "en";
      this.context.setCompanySettingsLanguage(lanSuffix);
    } else if (locale.includes("es_")) {
      lanSuffix = "es";
      this.context.setCompanySettingsLanguage(lanSuffix);
    } else if (locale.includes("fr_")) {
      lanSuffix = "fr";
      this.context.setCompanySettingsLanguage(lanSuffix);
    }
    const companyData = await Network.getCompanySettings(
      AppCompanyId,
      Platform.OS,
      this.context.languageCompanySettings
    );
    const companySettings = JSON.parse(JSON.stringify(companyData));

    this.context.changeActiveCompany({
      id: AppCompanyId,
      ...JSON.parse(JSON.stringify(companyData)),
    });

    this.context.handleCompanyColors(
      companySettings.skin.schema,
      companySettings.skin.primaryColor,
      companySettings.skin.backgroundColor,
      companySettings.skin.font
    );
  }

  async getData() {
    const companyId = isCustomApp() && AppCompanyId;
    const data = await Network.getUserData(companyId);
    console.log(data);
    return data;
  }

  formatUser(data) {
    return {
      displayName: data.display_name,
      profileImage: data.profile_image,
      role: data.role,
      work: data.work,
      followers: data.followers,
      live: data.live,
      email: data.email,
      sid: data.sid,
      creator: data.companies[0].creator,
    };
  }

  formatCompany(data, activeCompanySettings) {
    const { logo, default_video_image } = this.context.activeCompany;
    data.companies.forEach((c) => {
      c.logo = c.logo || logo;
      c.default_video_image = c.default_video_image || default_video_image;
      c.image = c.image || default_video_image;
    });
    const activeCompany = data.companies.find(
      (c: any) => c.id === data.id_company
    );
    return {
      companies: data.companies,
      activeCompany: {
        ...activeCompany,
        ...activeCompanySettings,
        id: data.id_company,
      },
    };
  }

  render() {
    return (
      <>
        <AppStatusBar />
        <View
          style={{
            flex: 1,
            alignItems: "center",
            justifyContent: "center",
            backgroundColor: this.context.backgroundColor,
            flexDirection: "column",
          }}
        >
          <OrientationLocker orientation={PORTRAIT} />
          {!this.state.verifyMailStep && (
            <Touchable onPress={() => console.log("")}>
              <Image
                resizeMode={"contain"}
                source={require("../../assets/images/logo_mini.png")}
                style={styles.logoStyle}
              />
            </Touchable>
          )}
          {this.state.verifyMailStep && (
            <>
              <Touchable onPress={() => console.log("")}>
                <Image
                  resizeMode={"contain"}
                  source={require("../../assets/images/logo_mini.png")}
                  style={styles.logoEmailStyle}
                />
              </Touchable>
              <AppText style={styles.emailTitle}>
                {global.strings.VerifyEmailError}
              </AppText>
              <AppText style={styles.emailSubTitle}>
                {global.strings.VerifyEmail}
              </AppText>
              <Touchable
                style={{
                  padding: 15,
                  backgroundColor: this.context.backgroundColor,
                  marginTop: 20,
                }}
                onPress={() =>
                  this.setState({ verifyMailStep: false }, () =>
                    this.initializeUser(null)
                  )
                }
              >
                <AppText style={styles.buttonText}>
                  {global.strings.TryAgain}
                </AppText>
              </Touchable>

              <Touchable
                onPress={async () => {
                  await Storage.deleteToken();
                  this.context.signedOut();
                  this.setState({ verifyMailStep: false }, () =>
                    this.initializeApp()
                  );
                }}
                style={{
                  position: "absolute",
                  bottom: 0,
                  justifyContent: "center",
                  padding: 5,
                  marginBottom: 80,
                  borderBottomColor: "white",
                  borderBottomWidth: 2,
                }}
              >
                <AppText style={styles.buttonTextLogout}>Logout</AppText>
              </Touchable>
            </>
          )}
        </View>
      </>
    );
  }
}

const styles = StyleSheet.create({
  logoStyle: {
    height: 134,
    width: 200,
  },

  logoEmailStyle: {
    height: 100,
    width: 100,
    marginBottom: 50,
  },

  emailTitle: {
    fontWeight: "700",
    fontSize: 25,
    marginBottom: 15,
  },

  emailSubTitle: {
    alignItems: "center",
    justifyContent: "center",
    marginLeft: 40,
    marginRight: 40,
    marginBottom: 20,
    textAlign: "center",
  },

  buttonText: {
    fontWeight: "700",
    fontSize: 19,
    paddingHorizontal: 15,
  },
  buttonTextLogout: {
    fontWeight: "700",
    fontSize: 19,
    paddingHorizontal: 15,
    justifyContent: "flex-end",
    alignSelf: "flex-end",
  },
});

export { Splashscreen };
