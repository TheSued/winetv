"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
exports.__esModule = true;
exports.LoginForm = void 0;
var react_1 = require("react");
var react_native_1 = require("react-native");
var providers_1 = require("../providers");
var components_1 = require("../components");
var common_1 = require("../components/common");
var Colors_1 = require("../environments/Colors");
var Storage_1 = require("../services/Storage");
var Network = require("../network");
var LoginForm = /** @class */ (function (_super) {
    __extends(LoginForm, _super);
    function LoginForm() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.state = {
            email: "",
            password: "",
            isLoading: true,
            isLogging: false,
            showError: false,
            message: "",
            anim: false,
            pageHasFocus: false
        };
        return _this;
    }
    LoginForm.prototype.componentDidMount = function () {
        var _this = this;
        this.focusListener = this.props.navigation.addListener("focus", function () {
            return _this.setState({ pageHasFocus: true });
        });
        this.blurListener = this.props.navigation.addListener("blur", function () {
            return _this.setState({ pageHasFocus: false });
        });
    };
    LoginForm.prototype.navigateToRegistration = function () {
        this.props.navigation.navigate("Registration");
    };
    LoginForm.prototype.navigateToHomepage = function () {
        this.props.navigation.navigate("Homepage");
    };
    LoginForm.prototype.onError = function (message) {
        var _this = this;
        this.setState({ message: message }, function () {
            return setTimeout(function () { return _this.setState({ showError: true }); }, 500);
        });
    };
    LoginForm.prototype.onSubmit = function () {
        return __awaiter(this, void 0, void 0, function () {
            var _this = this;
            return __generator(this, function (_a) {
                react_native_1.Keyboard.dismiss();
                if (this.state.email !== "" && this.state.password !== "") {
                    this.setState({ isLogging: true, message: "", showError: false });
                    setTimeout(function () { return __awaiter(_this, void 0, void 0, function () {
                        var token;
                        var _this = this;
                        return __generator(this, function (_a) {
                            switch (_a.label) {
                                case 0: return [4 /*yield*/, Network.login(this.state.email, this.state.password)];
                                case 1:
                                    token = _a.sent();
                                    if (!(token !== -1 && token !== -2)) return [3 /*break*/, 3];
                                    return [4 /*yield*/, Storage_1["default"].saveToken(token)];
                                case 2:
                                    _a.sent();
                                    this.context.signedOut();
                                    return [3 /*break*/, 4];
                                case 3:
                                    if (token === -1) {
                                        this.setState({ isLogging: false }, function () {
                                            return _this.onError(global.strings.CredentialsError);
                                        });
                                    }
                                    else {
                                        this.setState({ isLogging: false }, function () {
                                            return _this.onError(global.strings.NetworkError);
                                        });
                                    }
                                    _a.label = 4;
                                case 4: return [2 /*return*/];
                            }
                        });
                    }); }, 500);
                }
                else {
                    this.onError(global.strings.NoCredentials);
                }
                return [2 /*return*/];
            });
        });
    };
    LoginForm.prototype.renderView = function () {
        var _this = this;
        return (react_1["default"].createElement(react_native_1.View, { style: { flex: 1, alignItems: "center", justifyContent: "center" } },
            react_1["default"].createElement(react_native_1.Image, { resizeMode: "contain", source: { uri: "" + this.context.activeCompany.logo }, style: styles.imageStyle }),
            react_1["default"].createElement(common_1.Expand, { params: { start: 0, end: 30 }, controller: this.state.message == "" ? false : true, style: { marginTop: 13 } },
                react_1["default"].createElement(common_1.AppText, { style: styles.errorTextStyle }, this.state.showError ? this.state.message : "")),
            react_1["default"].createElement(react_native_1.View, { style: [
                    styles.formContainer1,
                    { backgroundColor: this.context.alphaColor },
                ] },
                react_1["default"].createElement(common_1.Input, { autoCapitalize: "none", autoCorrect: false, onChangeText: function (text) { return _this.setState({ email: text }); }, value: this.state.email, placeholder: global.strings.EmailPlaceholder, style: [
                        styles.formInput,
                        {
                            color: this.context.fontColor
                        },
                    ] })),
            react_1["default"].createElement(react_native_1.View, { style: [
                    styles.formContainer2,
                    { backgroundColor: this.context.alphaColor },
                ] },
                react_1["default"].createElement(common_1.Input, { onChangeText: function (text) { return _this.setState({ password: text }); }, value: this.state.password, placeholder: global.strings.PasswordPlaceholder, secureTextEntry: true, style: [
                        styles.formInput,
                        {
                            color: this.context.fontColor
                        },
                    ] })),
            react_1["default"].createElement(components_1.LoginButton, { color: this.context.fontColor, style: {
                    borderColor: this.context.buttonColor
                }, controller: this.state.isLogging, onSubmit: function () { return _this.onSubmit(); } }),
            this.context.activeCompany.registration_allowed && (react_1["default"].createElement(react_native_1.View, { style: { alignItems: "center" } },
                react_1["default"].createElement(common_1.AppText, { style: { marginTop: 30, color: this.context.fontColor } }, global.strings.NoAccount),
                react_1["default"].createElement(common_1.Touchable, { onPress: function () { return _this.navigateToRegistration(); } },
                    react_1["default"].createElement(common_1.AppText, { style: {
                            marginTop: 12,
                            fontWeight: "bold",
                            fontSize: 16,
                            textDecorationLine: "underline",
                            color: this.context.fontColor
                        } }, global.strings.SignUp)))),
            this.context.activeCompany.home_guest && (react_1["default"].createElement(common_1.Touchable, { style: {
                    alignItems: "center",
                    marginTop: 16,
                    color: this.context.fontColor
                }, onPress: function () { return _this.navigateToHomepage(); } },
                react_1["default"].createElement(common_1.AppText, { style: {
                        marginVertical: 12,
                        fontSize: 16,
                        color: this.context.fontColor
                    } }, global.strings.Skip))),
            react_1["default"].createElement(react_native_1.View, { style: [styles.footerContainer] },
                react_1["default"].createElement(common_1.AppText, { style: [styles.footerText, { color: this.context.fontColor }] }, global.strings.footerText),
                react_1["default"].createElement(react_native_1.Text, { style: [
                        styles.footerCompanyTitle,
                        { color: this.context.fontColor },
                    ] }, global.strings.footerCompanyTitle))));
    };
    LoginForm.prototype.render = function () {
        if (this.state.pageHasFocus) {
            return (react_1["default"].createElement(common_1.Background, { login: true, style: styles.backgroundStyle }, this.renderView()));
        }
        else {
            return react_1["default"].createElement(common_1.Background, { empty: true });
        }
    };
    LoginForm.contextType = providers_1.ActiveCompanyContext;
    return LoginForm;
}(react_1["default"].PureComponent));
exports.LoginForm = LoginForm;
var styles = react_native_1.StyleSheet.create({
    flex: {
        flex: 1
    },
    // logo
    backgroundStyle: {
        justifyContent: "space-around",
        alignItems: "center"
    },
    imageStyle: {
        height: 137,
        width: 220,
        alignSelf: "center",
        marginBottom: 24
    },
    // error text
    errorTextStyle: {
        alignSelf: "center",
        color: Colors_1["default"].TextError,
        fontSize: 17,
        elevation: 1
    },
    buttonContainer: {
        alignItems: "center",
        justifyContent: "center",
        marginTop: 25,
        borderWidth: 1,
        borderStyle: "solid",
        elevation: 1
    },
    buttonText: {
        fontSize: 22,
        paddingTop: 12,
        paddingBottom: 12,
        elevation: 1
    },
    formContainer1: {
        width: 300,
        marginTop: 13,
        borderTopLeftRadius: 5,
        borderTopRightRadius: 5
    },
    formContainer2: {
        width: 300,
        borderBottomLeftRadius: 5,
        borderBottomRightRadius: 5,
        marginTop: 2
    },
    formInput: {
        fontSize: 20,
        height: 60,
        paddingLeft: 15
    },
    logoStyle: {
        height: 134,
        width: 200
    },
    footerContainer: {
        textAlign: "center",
        marginTop: 100,
        display: "flex",
        justifyContent: "center",
        alignItems: "center"
    },
    footerText: {
        display: "flex",
        justifyContent: "center",
        alignItems: "center",
        fontSize: 15
    },
    footerCompanyTitle: {
        marginTop: 10,
        display: "flex",
        justifyContent: "center",
        alignItems: "center",
        fontSize: 11
    }
});
