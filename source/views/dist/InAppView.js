"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
var __spreadArrays = (this && this.__spreadArrays) || function () {
    for (var s = 0, i = 0, il = arguments.length; i < il; i++) s += arguments[i].length;
    for (var r = Array(s), k = 0, i = 0; i < il; i++)
        for (var a = arguments[i], j = 0, jl = a.length; j < jl; j++, k++)
            r[k] = a[j];
    return r;
};
exports.__esModule = true;
exports.InAppView = void 0;
var react_1 = require("react");
var react_native_iap_1 = require("react-native-iap");
var react_native_1 = require("react-native");
var CompanyProvider_1 = require("../providers/CompanyProvider");
var react_native_device_info_1 = require("react-native-device-info");
var Network = require("../network");
var InAppView = /** @class */ (function (_super) {
    __extends(InAppView, _super);
    function InAppView() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.state = {
            parentSubscriptions: [],
            parentProducts: [],
            childSubscription: [],
            childProduct: [],
            appState: react_native_1.AppState.currentState,
            loading: true
        };
        _this.purchaseUpdateSubscription = null;
        _this.purchaseErrorSubscription = null;
        _this.appStateSubscription = null;
        _this.validate_ANDROID = function (productId, purchaseToken, packageApp, id_series) { return __awaiter(_this, void 0, void 0, function () {
            var response, error_1;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        _a.trys.push([0, 2, , 3]);
                        return [4 /*yield*/, Network.validateIAP(productId, purchaseToken, packageApp, id_series)];
                    case 1:
                        response = _a.sent();
                        if (JSON.parse(JSON.stringify(response)).is_valid == "true") {
                            this.setState({ loading: false });
                            this.props.navigation.reset({
                                routes: [{ name: "Homepage" }]
                            });
                        }
                        return [3 /*break*/, 3];
                    case 2:
                        error_1 = _a.sent();
                        return [3 /*break*/, 3];
                    case 3: return [2 /*return*/];
                }
            });
        }); };
        _this.validate_IOS = function (purchaseToken, id_series) { return __awaiter(_this, void 0, void 0, function () {
            var response, error_2;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        _a.trys.push([0, 2, , 3]);
                        return [4 /*yield*/, Network.validateIosIAP(purchaseToken, id_series)];
                    case 1:
                        response = _a.sent();
                        if (JSON.parse(JSON.stringify(response)).is_valid == "true") {
                            this.setState({ loading: false });
                            this.props.navigation.reset({
                                routes: [{ name: "Homepage" }]
                            });
                        }
                        return [3 /*break*/, 3];
                    case 2:
                        error_2 = _a.sent();
                        return [3 /*break*/, 3];
                    case 3: return [2 /*return*/];
                }
            });
        }); };
        _this.renderItem = function (_a) {
            var item = _a.item;
            var _parentPayments = _this.props.route.params._parentPayments;
            var parent = function () {
                if (_parentPayments.length > 0) {
                    return _parentPayments.filter(function (product) {
                        return product.product === item.productId ||
                            product.subscription === item.productId;
                    });
                }
                else {
                    return [];
                }
            };
            return (react_1["default"].createElement(react_native_1.View, { style: [
                    styles.productsContainer,
                    {
                        backgroundColor: _this.context.backgroundColor + "40"
                    },
                ] },
                react_1["default"].createElement(react_native_1.View, { style: { flex: 2, marginVertical: 4 } },
                    react_1["default"].createElement(react_native_1.Text, { style: {
                            color: _this.context.buttonColor,
                            fontSize: 22,
                            fontWeight: "bold"
                        } }, item.title)),
                react_1["default"].createElement(react_native_1.View, { style: { flex: 2, marginVertical: 4 } },
                    react_1["default"].createElement(react_native_1.Text, { style: { color: _this.context.fontColor } }, parent().length ? parent()[0].description : item.description)),
                react_1["default"].createElement(react_native_1.View, { style: { flex: 2, marginVertical: 4, width: "100%" } },
                    react_1["default"].createElement(react_native_1.Pressable, { onPress: function () { return __awaiter(_this, void 0, void 0, function () {
                            var err_1, err_2;
                            return __generator(this, function (_a) {
                                switch (_a.label) {
                                    case 0:
                                        this.setState({ loading: true });
                                        if (!(this.state.loading === true)) return [3 /*break*/, 5];
                                        _a.label = 1;
                                    case 1:
                                        _a.trys.push([1, 3, , 4]);
                                        return [4 /*yield*/, react_native_iap_1["default"].requestSubscription(item.productId)];
                                    case 2:
                                        _a.sent();
                                        return [3 /*break*/, 4];
                                    case 3:
                                        err_1 = _a.sent();
                                        ("erore nel request product");
                                        console.warn(err_1.code, err_1.message);
                                        return [3 /*break*/, 4];
                                    case 4: return [3 /*break*/, 9];
                                    case 5:
                                        if (!(item.type === "subs" || item.type === "sub")) return [3 /*break*/, 9];
                                        _a.label = 6;
                                    case 6:
                                        _a.trys.push([6, 8, , 9]);
                                        return [4 /*yield*/, react_native_iap_1["default"].requestSubscription(item.productId)];
                                    case 7:
                                        _a.sent();
                                        return [3 /*break*/, 9];
                                    case 8:
                                        err_2 = _a.sent();
                                        console.warn(err_2.code, err_2.message);
                                        return [3 /*break*/, 9];
                                    case 9: return [2 /*return*/];
                                }
                            });
                        }); }, style: function (_a) {
                            var pressed = _a.pressed;
                            return [
                                {
                                    backgroundColor: pressed
                                        ? _this.context.buttonColor
                                        : _this.context.buttonColor,
                                    padding: 10,
                                    opacity: pressed ? 0.4 : 1,
                                    width: "45%",
                                    justifyContent: "center",
                                    alignItems: "center",
                                    borderRadius: 4
                                },
                            ];
                        } },
                        react_1["default"].createElement(react_native_1.Text, { style: {
                                color: _this.context.buttonTextColor,
                                fontWeight: "bold"
                            } }, item.localizedPrice)))));
        };
        _this.componentDidMount = function () { return __awaiter(_this, void 0, void 0, function () {
            var _a, _parentPayments, _childSubscription, _childProduct;
            var _this = this;
            return __generator(this, function (_b) {
                switch (_b.label) {
                    case 0:
                        this.appStateSubscription = react_native_1.AppState.addEventListener("change", function (nextAppState) {
                            if (_this.state.appState.match(/background/) &&
                                nextAppState === "active") {
                                _this.props.navigation.pop();
                            }
                            _this.setState({ appState: nextAppState });
                        });
                        _a = this.props.route.params, _parentPayments = _a._parentPayments, _childSubscription = _a._childSubscription, _childProduct = _a._childProduct;
                        return [4 /*yield*/, react_native_iap_1["default"].initConnection().then(function () { return __awaiter(_this, void 0, void 0, function () {
                                var childSubscription, err_3, childProduct, err_4, parentSubsIds, parentSubscriptions, err_5, parentProdsIds, parentProducts, err_6;
                                var _this = this;
                                return __generator(this, function (_a) {
                                    switch (_a.label) {
                                        case 0:
                                            _a.trys.push([0, 3, , 4]);
                                            if (!!/^\s*$/.test(_childSubscription)) return [3 /*break*/, 2];
                                            return [4 /*yield*/, react_native_iap_1["default"].getSubscriptions([
                                                    _childSubscription,
                                                ])];
                                        case 1:
                                            childSubscription = _a.sent();
                                            this.setState({ childSubscription: childSubscription });
                                            _a.label = 2;
                                        case 2: return [3 /*break*/, 4];
                                        case 3:
                                            err_3 = _a.sent();
                                            console.warn(err_3); // standardized err.code and err.message available
                                            return [3 /*break*/, 4];
                                        case 4:
                                            _a.trys.push([4, 7, , 8]);
                                            if (!!/^\s*$/.test(_childProduct)) return [3 /*break*/, 6];
                                            return [4 /*yield*/, react_native_iap_1["default"].getProducts([
                                                    _childProduct,
                                                ])];
                                        case 5:
                                            childProduct = _a.sent();
                                            this.setState({ childProduct: childProduct });
                                            _a.label = 6;
                                        case 6: return [3 /*break*/, 8];
                                        case 7:
                                            err_4 = _a.sent();
                                            console.warn(err_4); // standardized err.code and err.message available
                                            return [3 /*break*/, 8];
                                        case 8:
                                            _a.trys.push([8, 11, , 12]);
                                            if (!_parentPayments.length) return [3 /*break*/, 10];
                                            console.log("parente", _parentPayments);
                                            parentSubsIds = _parentPayments
                                                .filter(function (item) {
                                                return !/^\s*$/.test(item.subscription);
                                            })
                                                .map(function (item) {
                                                return item.subscription;
                                            });
                                            if (!(parentSubsIds.length > 0)) return [3 /*break*/, 10];
                                            return [4 /*yield*/, react_native_iap_1["default"].getSubscriptions(parentSubsIds)];
                                        case 9:
                                            parentSubscriptions = _a.sent();
                                            this.setState({ parentSubscriptions: parentSubscriptions });
                                            _a.label = 10;
                                        case 10: return [3 /*break*/, 12];
                                        case 11:
                                            err_5 = _a.sent();
                                            console.warn(err_5); // standardized err.code and err.message available
                                            return [3 /*break*/, 12];
                                        case 12:
                                            _a.trys.push([12, 15, , 16]);
                                            if (!_parentPayments.length) return [3 /*break*/, 14];
                                            parentProdsIds = _parentPayments
                                                .filter(function (item) {
                                                return !/^\s*$/.test(item.product);
                                            })
                                                .map(function (item) {
                                                return item.product;
                                            });
                                            if (!(parentProdsIds.length > 0)) return [3 /*break*/, 14];
                                            return [4 /*yield*/, react_native_iap_1["default"].getProducts(parentProdsIds)];
                                        case 13:
                                            parentProducts = _a.sent();
                                            this.setState({ parentProducts: parentProducts });
                                            _a.label = 14;
                                        case 14: return [3 /*break*/, 16];
                                        case 15:
                                            err_6 = _a.sent();
                                            console.warn(err_6); // standardized err.code and err.message available
                                            return [3 /*break*/, 16];
                                        case 16:
                                            this.setState({ loading: false });
                                            this.purchaseUpdateSubscription = react_native_iap_1.purchaseUpdatedListener(function (purchase) { return __awaiter(_this, void 0, void 0, function () {
                                                var id_series, bundleID, receipt;
                                                return __generator(this, function (_a) {
                                                    id_series = this.props.route.params.id_series;
                                                    bundleID = react_native_device_info_1.getBundleId();
                                                    receipt = react_native_1.Platform.OS === "ios"
                                                        ? purchase.transactionReceipt
                                                        : react_native_1.Platform.OS === "android"
                                                            ? JSON.parse(purchase.transactionReceipt)
                                                            : null;
                                                    if (receipt) {
                                                        if (react_native_1.Platform.OS === "ios") {
                                                            this.validate_IOS(receipt, id_series);
                                                        }
                                                        else if (react_native_1.Platform.OS === "android") {
                                                            this.validate_ANDROID(receipt["productId"], receipt["purchaseToken"], bundleID, id_series);
                                                        }
                                                        react_native_iap_1["default"].finishTransaction(purchase);
                                                    }
                                                    return [2 /*return*/];
                                                });
                                            }); });
                                            this.purchaseErrorSubscription = react_native_iap_1.purchaseErrorListener(function (error) {
                                                console.warn("purchaseErrorListener", error);
                                                _this.setState({ loading: false });
                                            });
                                            return [2 /*return*/];
                                    }
                                });
                            }); })];
                    case 1:
                        _b.sent();
                        return [2 /*return*/];
                }
            });
        }); };
        return _this;
    }
    InAppView.prototype.componentWillUnmount = function () {
        var _this = this;
        react_native_1.AppState.removeEventListener("change", function (nextAppState) {
            if (_this.state.appState.match(/background/) &&
                nextAppState === "active") {
                _this.props.navigation.pop();
            }
            _this.setState({ appState: nextAppState });
        });
        if (this.purchaseUpdateSubscription) {
            this.purchaseUpdateSubscription.remove();
            this.purchaseUpdateSubscription = null;
        }
        if (this.purchaseErrorSubscription) {
            this.purchaseErrorSubscription.remove();
            this.purchaseErrorSubscription = null;
        }
        react_native_iap_1["default"].endConnection();
    };
    InAppView.prototype.render = function () {
        return !this.state.loading ? (this.state.parentSubscriptions.length > 0 ||
            this.state.childProduct ||
            this.state.childSubscription ||
            this.state.parentProducts ? (react_1["default"].createElement(react_native_1.View, { style: [
                { backgroundColor: this.context.backgroundColor },
                styles.container,
            ] },
            react_1["default"].createElement(react_native_1.View, { style: {
                    flex: 1,
                    alignItems: "center",
                    justifyContent: "center",
                    width: "100%",
                    height: "100%"
                } },
                react_1["default"].createElement(OrientationLocker, { orientation: PORTRAIT }),
                react_1["default"].createElement(react_native_1.View, { style: {
                        borderBottomColor: this.context.fontColor,
                        borderStyle: "solid",
                        borderBottomWidth: 0.5,
                        padding: 5,
                        width: "90%",
                        height: "100%",
                        justifyContent: "center",
                        alignItems: "center"
                    } },
                    react_1["default"].createElement(react_native_1.Text, { style: [
                            styles.mainHeader,
                            {
                                color: this.context.fontColor
                            },
                        ] }, global.strings.ChoosePlan))),
            react_1["default"].createElement(react_native_1.View, { style: { flex: 5 } },
                react_1["default"].createElement(react_native_1.FlatList, { style: styles.flatList, data: __spreadArrays(this.state.childSubscription, this.state.childProduct, this.state.parentSubscriptions, this.state.parentProducts), renderItem: this.renderItem, keyExtractor: function (item) { return item.productId; } })))) : (react_1["default"].createElement(react_native_1.View, { style: [
                { backgroundColor: this.context.backgroundColor },
                styles.container,
            ] },
            react_1["default"].createElement(OrientationLocker, { orientation: PORTRAIT }),
            react_1["default"].createElement(react_native_1.ActivityIndicator, { size: "large", color: this.context.buttonColor || "black" })))) : (react_1["default"].createElement(react_native_1.View, { style: [
                { backgroundColor: this.context.backgroundColor },
                styles.container,
            ] },
            react_1["default"].createElement(OrientationLocker, { orientation: PORTRAIT }),
            react_1["default"].createElement(react_native_1.ActivityIndicator, { size: "large", color: this.context.buttonColor || "black" })));
    };
    InAppView.contextType = CompanyProvider_1.ActiveCompanyContext;
    return InAppView;
}(react_1.Component));
exports.InAppView = InAppView;
var styles = react_native_1.StyleSheet.create({
    container: {
        flex: 1,
        alignItems: "center",
        justifyContent: "center"
    },
    title: {
        fontSize: 22,
        color: "white"
    },
    mainHeader: {
        fontSize: 25,
        fontWeight: "bold"
    },
    flatList: { width: "100%", marginTop: 20 },
    productsContainer: {
        borderRadius: 4,
        justifyContent: "center",
        marginVertical: 8,
        marginHorizontal: 16,
        padding: 20,
        shadowOffset: { width: 0, height: 1 },
        shadowOpacity: 0.8,
        shadowRadius: 1,
        elevation: 15
    }
});
