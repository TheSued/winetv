import React from "react";
import { FlatList, StyleSheet, View, Animated } from "react-native";

import Icon from "react-native-vector-icons/MaterialIcons";
import { ActiveCompanyContext } from "../providers/CompanyProvider";
import { PulseIndicator } from "react-native-indicators";
import { HomePageList } from "../components";
import * as Network from "../network";
import Colors from "../environments/Colors";
import { AppText, Background, Input, Touchable } from "../components/common";
import { Header } from "../components";
import { formatVideoObject } from "../utils/common.utils";
import Tabs from "../views/Tabs";
import {
  OrientationLocker,
  PORTRAIT,
  LANDSCAPE,
} from "react-native-orientation-locker";
const currentScreen = "SearchPage";

class Searchpage extends React.PureComponent {
  static contextType = ActiveCompanyContext;

  constructor(props: any) {
    super(props);
    this.state = {
      searchText: "",
      selectedCategories: [],
      isLoading: true,
      isSelectingCategories: false,

      categories: [],

      listContent: [],
      searchDataSeries: {
        data: [],
        step: 1,
      },
      searchDataVideos: {
        data: [],
        step: 1,
      },
      active: 0,
    };
  }

  componentDidMount() {
    this.getCategories();
  }

  async getCategories() {
    const categories = await Network.getCategories();
    this.setState({
      categories: categories.filter((cat) => cat.show == true),
      isLoading: false,
    });
    const catId = this.props.route.params["id_cat"] * 1;
    catId && this.toggleCategory(catId, true);
  }

  async loadSearchData() {
    const { activeCompany } = this.context;
    const categories = this.state.selectedCategories.map((el) => el.id);

    this.setState({ isLoading: true }, async () => {
      const promises = await Promise.all([
        Network.searchSeries(
          activeCompany.id,
          1,
          this.state.searchText,
          categories
        ),
        Network.searchVideos(
          activeCompany.id,
          1,
          this.state.searchText,
          categories
        ),
      ]);
      const responseSeries = promises[0].map((el) => ({
        ...el,
        type: "serie",
      }));
      const responseVideos = promises[1].map((el) => ({
        ...el,
        type: "video",
      }));

      const searchDataSeries: any = { step: 1, data: [] };
      const searchDataVideos: any = { step: 1, data: [] };

      if (responseSeries && responseSeries.length >= global.videoEachStep) {
        searchDataSeries.step += 1;
      }
      searchDataSeries.data = responseSeries || [];

      if (responseVideos && responseVideos.length >= global.videoEachStep) {
        searchDataVideos.step += 1;
      }
      searchDataVideos.data = responseVideos || [];
      /*
      this.setState({ isLoading: false, searchDataSeries, searchDataVideos, listContent: [...searchDataSeries.data, ...searchDataVideos.data] });
      */

      this.setState({
        isLoading: false,
        searchDataSeries,
        searchDataVideos,
        listContent: [...searchDataSeries.data, ...searchDataVideos.data],
      });
    });
  }

  async searchMoreSeries() {
    const { searchDataSeries, searchText } = this.state;
    const categoriesSearchMore = this.state.selectedCategories.map(
      (el) => el.id
    );
    let step = searchDataSeries.step;

    if (searchDataSeries.data.length % global.videoEachStep === 0) {
      const response = await Network.searchSeries(
        this.context.activeCompany.id,
        step,
        searchText,
        categoriesSearchMore
      );
      if (response.length) {
        if (response.length >= global.videoEachStep) {
          step = step + 1;
        }
        this.setState({
          searchDataSeries: {
            data: [...searchDataSeries.data, ...response],
            step,
          },
        });
      }
    }
  }

  async searchMoreVideos() {
    const { searchDataVideos, searchText } = this.state;
    const categoriesSearchMore = this.state.selectedCategories.map(
      (el) => el.id
    );
    let step = searchDataVideos.step;
    if (searchDataVideos.data.length % global.videoEachStep === 0) {
      const response = await Network.searchVideos(
        this.context.activeCompany.id,
        step,
        searchText,
        categoriesSearchMore
      );
      if (response.length) {
        if (response.length >= global.videoEachStep) {
          step = step + 1;
        }
        this.setState({
          searchDataVideos: {
            data: [...searchDataVideos.data, ...response],
            step,
          },
        });
      }
    }
  }

  toggleCategory(id: string, refresh: boolean) {
    const categories = this.state.categories;
    const category = categories.find((el) => el.id === id);
    category.selected = !category.selected;
    this.setState({
      categories,
      selectedCategories: categories.filter((el) => el.selected),
    });

    if (refresh) {
      this.setState(
        { selectedCategories: categories.filter((el) => el.selected) },
        () => {
          this.loadSearchData();
        }
      );
    }
  }

  //player e video
  openSeries(serie) {
    this.props.navigation.navigate("SerieView", {
      serie: serie,
      previousScreen: currentScreen,
    });
  }

  openElement(element: any) {
    if (element.type === "video") {
      this.openPlayer(element);
    } else {
      this.openSeries(element);
    }
  }

  async openPlayer(item: any) {
    if (item.quiz) {
      this.setState({ quizAlert: true });
    } else {
      this.context.setActualPreview(item.img_preview);
      this.setState({ loadingInfoVideo: true });
      const promises = await Promise.all([
        Network.getVideoInfo(item.id), // needed in case we open a video from notification
        Network.getVideoSource(item.id),
        Network.getAttachments(item.id),
      ]);
      const videoInfo = promises[0];
      const data = promises[1];
      const attachments = promises[2];
      if (data && data.status === "success") {
        let video = formatVideoObject(data);
        if (!videoInfo.ondemand) {
          video.started = videoInfo.livenow;
        }
        this.props.navigation.navigate("Player", {
          previousScreen: currentScreen,
          video: {
            id: item.id,
            creator: item.display_name
              ? item.display_name
              : videoInfo.display_name, // ? needed in case we open a video from notification
            title: item.title ? item.title : videoInfo.title, // ? needed in case we open a video from notification
            description: item.description
              ? item.description
              : videoInfo.description, // ? needed in case we open a video from notification
            source: video,
            attachments: attachments,
          },
        });
      } else if (
        data.status == "error_access_not_permit" &&
        data.id_series &&
        data.id_series != ""
      ) {
        // user need to pay the serie containing the video
        const serieData = await Network.getSerieInfo(data.id_series);
        this.setState({ loadingInfoVideo: false }, () =>
          this.props.navigation.navigate("SerieView", {
            serie: serieData[0],
            previousScreen: currentScreen,
          })
        );
      } else {
        this.onError(global.strings.NoVideoAccess);
      }
      if (this.state.loadingInfoVideo) {
        // needed in case we are in SerieView already
        this.setState({ loadingInfoVideo: false });
      }
    }
  }

  render() {
    const {
      isLoading,
      isSelectingCategories,
      categories,
      searchText,
      selectedCategories,
      active,
    } = this.state;
    const {
      viewPadding,
      categoriesButtonStyle,
      categoriesButtonLabelStyle,
      inputStyle,
      flexCentered,
      categoryItemStyle,
      categoryItemSelected,
      selectedCategoryItemStyle,
      selectedCategoriesContainerStyle,
      selectedCategoryTextStyle,
      listStyle,
    } = searchpageStyles;

    return (
      <Background
        simple
        header={<Header onMenuPress={() => this.props.navigation.goBack()} />}
      >
        <View style={viewPadding}>
          <OrientationLocker orientation={PORTRAIT} />
          <View style={{ flexDirection: "row", paddingTop: 8 }}>
            <Input
              style={[inputStyle, {}]}
              onChangeText={(text: string) =>
                this.setState({ searchText: text })
              }
              value={searchText}
              returnKeyType={"search"}
              placeholder={global.strings.SearchPlaceholder}
              onSubmitEditing={() => this.loadSearchData()}
            />

            {!isSelectingCategories && categories.length > 0 && (
              <Touchable
                style={categoriesButtonStyle}
                onPress={() => this.setState({ isSelectingCategories: true })}
              >
                <AppText style={categoriesButtonLabelStyle}>
                  {global.strings.SearchCategory}
                </AppText>
                <Icon name="arrow-drop-down" color="white" size={25} />
              </Touchable>
            )}

            {isSelectingCategories && categories.length > 0 && (
              <Touchable
                style={categoriesButtonStyle}
                onPress={() =>
                  this.setState(
                    {
                      isSelectingCategories: false,
                      selectedCategories: this.state.categories.filter(
                        (el) => el.selected
                      ),
                    },
                    () => this.loadSearchData()
                  )
                }
              >
                <AppText style={categoriesButtonLabelStyle}>
                  {global.strings.ConfirmCategory}
                </AppText>
                <Icon name="done" size={19} color="white" />
              </Touchable>
            )}
          </View>

          {isLoading && (
            <View style={flexCentered}>
              <PulseIndicator
                size={50}
                color={this.context.activeCompany.font_color || Colors.Text1}
              />
            </View>
          )}

          {!isLoading && (
            <>
              {!isSelectingCategories && (
                <>
                  <View style={selectedCategoriesContainerStyle}>
                    {selectedCategories.map((el, index) => (
                      <Touchable
                        style={selectedCategoryItemStyle}
                        key={index}
                        onPress={() => this.toggleCategory(el.id, true)}
                      >
                        <AppText style={selectedCategoryTextStyle}>
                          {el.name}
                        </AppText>
                        <Icon name="close" size={18} color="white" />
                      </Touchable>
                    ))}
                  </View>

                  {!this.context.activeCompany.hide_search_video && (
                    <>
                      <View
                        style={{
                          flexDirection: "row",
                          justifyContent: "space-around",
                          marginBottom: 8,
                        }}
                      >
                        <Touchable
                          style={{
                            backgroundColor:
                              active === 0 ? "rgb(179,179,179)" : "#fff",
                            flex: 1,
                            alignItems: "center",
                            padding: 10,
                            borderRadius: 5,
                          }}
                          onPress={() => this.setState({ active: 0 })}
                        >
                          <AppText
                            style={{
                              color: active === 0 ? "#fff" : "black",
                            }}
                          >
                            Serie
                          </AppText>
                        </Touchable>

                        <Touchable
                          style={{
                            backgroundColor:
                              active === 1 ? "rgb(179,179,179)" : "#fff",
                            flex: 1,
                            alignItems: "center",
                            padding: 10,
                            borderRadius: 5,
                          }}
                          onPress={() => this.setState({ active: 1 })}
                        >
                          <AppText
                            style={{
                              color: active === 1 ? "#fff" : "black",
                            }}
                          >
                            Video
                          </AppText>
                        </Touchable>
                      </View>

                      <View
                        style={{ flex: 1, width: "100%", flexWrap: "nowrap" }}
                      >
                        {active === 0 && (
                          <HomePageList
                            listStyle={{
                              marginTop:
                                Math.ceil(
                                  this.state.selectedCategories.length / 2
                                ) * 15,
                            }}
                            horizontal={false}
                            data={this.state.searchDataSeries.data}
                            onRef={(ref) => (this.onItemPress = ref)}
                            onItemPress={this.openElement.bind(this)}
                            onEndReachedThreshold={10}
                            onEndReached={() => this.searchMoreSeries()}
                            imageSize={{
                              width: global.screenProp.width - 12,
                              height: ((global.screenProp.width - 12) / 16) * 9,
                            }}
                            itemStyle={{ marginBottom: 15 }}
                            style={{ width: "100%" }}
                          />
                        )}
                        {active === 1 && (
                          <HomePageList
                            listStyle={{
                              marginTop:
                                Math.ceil(
                                  this.state.selectedCategories.length / 2
                                ) * 15,
                            }}
                            horizontal={false}
                            data={this.state.searchDataVideos.data}
                            onRef={(ref) => (this.onItemPress = ref)}
                            onItemPress={this.openElement.bind(this)}
                            onEndReachedThreshold={10}
                            onEndReached={() => this.searchMoreVideos()}
                            imageSize={{
                              width: global.screenProp.width - 12,
                              height: ((global.screenProp.width - 12) / 16) * 9,
                            }}
                            itemStyle={{ marginBottom: 15 }}
                            style={{ width: "100%" }}
                          />
                        )}
                      </View>
                    </>
                  )}
                  {this.context.activeCompany.hide_search_video && (
                    <HomePageList
                      listStyle={{
                        marginTop:
                          Math.ceil(this.state.selectedCategories.length / 2) *
                          15,
                        marginBottom: 50,
                      }}
                      horizontal={false}
                      data={this.state.searchDataSeries.data}
                      onRef={(ref) => (this.onItemPress = ref)}
                      onItemPress={this.openElement.bind(this)}
                      onEndReachedThreshold={10}
                      onEndReached={() => this.searchMoreSeries()}
                      imageSize={{
                        width: global.screenProp.width - 12,
                        height: ((global.screenProp.width - 12) / 16) * 9,
                      }}
                      itemStyle={{ marginBottom: 15 }}
                      style={{ width: "100%" }}
                    />
                  )}
                </>
              )}

              {isSelectingCategories && (
                <FlatList
                  style={listStyle}
                  data={categories}
                  renderItem={({ item, index }) => (
                    <Touchable
                      onPress={() => this.toggleCategory(item.id, false)}
                      style={categoryItemStyle}
                    >
                      <AppText
                        style={item.selected ? categoryItemSelected : {}}
                      >
                        {item.name}
                      </AppText>
                      {item.selected && (
                        <Icon name="done" color="rgb(179,179,179)" size={16} />
                      )}
                    </Touchable>
                  )}
                />
              )}
            </>
          )}
        </View>
      </Background>
    );
  }
}

const searchpageStyles = StyleSheet.create({
  flex1: { flex: 1 },
  flexCentered: {
    flex: 1,
    alignItems: "center",
    justifyContent: "center",
  },
  listStyle: {
    flex: 1,
    marginTop: 4,
  },
  inputStyle: {
    backgroundColor: "rgb(179,179,179)",
    padding: 11,
    borderRadius: 5,
    fontSize: 18,
    flex: 4,
    flexDirection: "row",
    marginHorizontal: 2,
    color: "white",
  },
  viewPadding: {
    paddingHorizontal: 6,
    flex: 1,
  },
  categoriesButtonStyle: {
    flex: 2,
    marginHorizontal: 2,
    alignItems: "center",
    flexDirection: "row",
    paddingVertical: 12,
    paddingHorizontal: 8,
    backgroundColor: "rgb(179,179,179)",
    color: "white",

    borderRadius: 6,
  },
  categoriesButtonLabelStyle: {
    fontSize: 18,
    fontWeight: "600",
    color: "white",
  },

  categoryItemStyle: {
    flex: 1,
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center",
    marginVertical: 4,
    paddingVertical: 6,
  },

  categoryItemSelected: {
    color: "rgb(179,179,179)",
  },
  selectedCategoryItemStyle: {
    flexDirection: "row",
    alignItems: "center",
    paddingVertical: 2,
    paddingHorizontal: 8,
    borderRadius: 16,
    margin: 8,
    backgroundColor: " rgb(179,179,179)",
  },
  selectedCategoriesContainerStyle: {
    flexDirection: "row",
    flexWrap: "wrap",
    marginTop: 5,
  },
  selectedCategoryTextStyle: {
    marginRight: 4,
    color: "white",
  },
});

export { Searchpage };
