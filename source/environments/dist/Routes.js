"use strict";
exports.__esModule = true;
var AppEnvironment_1 = require("./AppEnvironment");
var react_native_1 = require("react-native");
var BaseRoute = "https://teyuto.tv/app";
//const BaseRoute =
//'https://20211103t163136-dot-teyuto-live.ey.r.appspot.com/';
var ApiRoute = BaseRoute + "/api/v1";
// const CartApiRoute = `${CartBaseRoute}/api/v1/business`;
var appSuffix = "&app=1";
var companyIdSuffix = "&cid=" + AppEnvironment_1.AppCompanyId;
var CartBaseRoute = "https://testarea.teyuto.tv/app/api/v1/business/?f=get_custom_field";
var videoApiRoute = "https://testarea.teyuto.tv/app/api/v1";
exports["default"] = Object.freeze({
    // Assets
    NoProfileImg: BaseRoute + "/img/no_img_profile_u.png",
    // User
    Login: ApiRoute + "/user/?f=login" + appSuffix + companyIdSuffix,
    SetDeviceToken: ApiRoute + "/user/?f=set_device_token" + appSuffix,
    ProfileDataRoute: ApiRoute + "/user/?f=user_data" + appSuffix,
    changeCompany: ApiRoute + "/user/?f=change_company" + appSuffix,
    GetPaymentInfo: ApiRoute + "/user/?f=get_last_info_pay" + appSuffix,
    Register: ApiRoute + "/user/?f=register" + appSuffix + companyIdSuffix,
    ChangeLanguage: ApiRoute + "/user/?f=update_user_language",
    //Company
    CompanySettings: ApiRoute + "/user/?f=settings_company" + appSuffix,
    HomepageSettings: ApiRoute + "/landing/?f=get_homepage_settings" + appSuffix,
    // Serie
    GetCollections: ApiRoute + "/collection/?f=get_collection_user" + appSuffix,
    GetSeries: "https://20211123t162221-dot-teyuto-live.ey.r.appspot.com/app/api/v1/collection/?f=get_collection" + appSuffix + "&platform=" + react_native_1.Platform.OS,
    GetBundleChild: ApiRoute + "/collection/?f=get_child_series" + appSuffix,
    GetSerieParent: ApiRoute + "/collection/?f=get_parent_series" + appSuffix,
    // Video
    GetVideo: ApiRoute + "/video/?f=get_video" + appSuffix,
    GetVideoKeepWatching: ApiRoute + "/video/?f=get_keep_watching" + appSuffix,
    AddLive: ApiRoute + "/video/?f=add_live" + appSuffix,
    GetAttachments: ApiRoute + "/video/?f=get_file" + appSuffix,
    VideoSource: ApiRoute + "/video/?f=get_video_source" + appSuffix,
    PlayVideo: ApiRoute + "/video/?f=action_enter" + appSuffix,
    UpdateVideo: ApiRoute + "/video/?f=action_update" + appSuffix,
    ViewersCounter: ApiRoute + "/video/?f=info_view" + appSuffix,
    GetVideoTest: videoApiRoute + "/video/?f=test_get_video",
    GetVideoTestPreview: ApiRoute + "/messenger/?f=get_preview_url" + appSuffix,
    GetVideoRelatedPreview: ApiRoute + "/messenger/?f=get_preview_url" + appSuffix,
    GetPreviewUrl: ApiRoute + "/messenger/?f=get_preview_url" + appSuffix,
    AddLike: ApiRoute + "/video/?f=set_likes" + appSuffix,
    GetSubtitles: ApiRoute + "/video/?f=get_subtitles&video=",
    // Category
    GetCategories: ApiRoute + "/categories/?f=get_categories&show" + appSuffix,
    // StreamForm Page Routes
    AddLiveWithCover: ApiRoute + "/google/?f=secureUrl&url=/app/api/v1/video/?f=add_live" + appSuffix,
    // Chat
    ListChat: ApiRoute + "/video_chat/?f=list_chat" + appSuffix,
    ListPartecipants: ApiRoute + "/video_chat/?f=list_live_partecipants" + appSuffix,
    ListQeA: ApiRoute + "/video_chat/?f=list_qea_ondemand" + appSuffix,
    SendMessage: ApiRoute + "/video_chat/?f=send_message" + appSuffix,
    SendQeA: ApiRoute + "/video_chat/?f=send_qea_ondemand" + appSuffix,
    AnswerQuestion: ApiRoute + "/video_chat/?f=set_qea" + appSuffix,
    // Payments
    // Pay: `https://20200502t210227-dot-teyuto-live.ey.r.appspot.com/app/api/v1.1/pay/?f=create_payment${appSuffix}`, // create a paymentIntent
    Pay: "https://teyuto.tv/app/api/v1.1/pay/?f=create_payment" + appSuffix,
    ApplyCoupon: ApiRoute + "/pay/?f=apply_coupon_series" + appSuffix,
    DetailPricing: "https://testarea.teyuto.tv/app/api/v1.1/pay/?f=details_pricing_preview",
    // Quiz
    Quiz: BaseRoute + "/live/quiz",
    CustCart: CartBaseRoute + "\u200B"
});
