
export default Object.freeze({
    const light = {
        theme: {
            background: '#fff',
            border: 'rgb(48,48,48)',
            backgroundAlt: '#eaeaeb',
            borderAlt: 'rgb(48,48,48)',
            text: 'rgb(48,48,48)',
        }
    }

})