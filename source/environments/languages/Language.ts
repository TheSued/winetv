import { Platform, NativeModules } from "react-native";

import Strings_en_GB from "./Strings-en_GB";
import Strings_it_IT from "./Strings-it_IT";
import Strings_es_ES from "./Strings-es_ES";
import Strings_fr_FR from "./Strings-fr_FR";

export default function defineLanguage() {
  const locale = getLocale();
  if (locale.includes("it_")) {
    return Strings_it_IT;
  } else if (locale.includes("en_")) {
    return Strings_en_GB; // default - english
  } else if (locale.includes("es_")) {
    return Strings_es_ES;
  } // default - english
  else if (locale.includes("fr_")) {
    return Strings_fr_FR; // default - english
  }
}

function getLocale(): string {
  switch (Platform.OS) {
    case "ios":
      return (
        (NativeModules.SettingsManager.settings.AppleLocale &&
          NativeModules.SettingsManager.settings.AppleLocale.replace(
            "-",
            "_"
          )) ||
        NativeModules.SettingsManager.settings.AppleLanguages[0]
      );
    case "android":
      return NativeModules.I18nManager.localeIdentifier;
  }
  return "";
}
