import React from "react";
import { Platform } from "react-native";
import defineLanguage from "../environments/languages/Language";
import Strings_en_GB from "../environments/languages/Strings-en_GB";
import Strings_it_IT from "../environments/languages/Strings-it_IT";
import Strings_es_ES from "../environments/languages/Strings-es_ES";
import Strings_fr_FR from "../environments/languages/Strings-fr_FR";
import { AppCompanyId, isCustomApp } from "../environments/AppEnvironment";
import { getUserData } from "../network/user";

export const UtilsContext = React.createContext({});

interface UtilsState {
  font: string;
  strings: any;
  paymentSuccessfull: boolean;
  videoNotificationId: number;
}

export class UtilsProvider extends React.PureComponent<{}, UtilsState> {
  componentWillMount = async () => {
    const companyId = isCustomApp() && AppCompanyId;
    const data = await getUserData(companyId);
    if (data && data.language) {
      if (data.language === "en") {
        this.setState({ strings: Strings_en_GB });
      } else if (data.language === "it") {
        this.setState({ strings: Strings_it_IT });
      } else if (data.language === "es") {
        this.setState({ strings: Strings_es_ES });
      } else if (data.language === "fr") {
        this.setState({ strings: Strings_fr_FR });
      }
    }
  };
  constructor(props: any) {
    super(props);
    this.state = {
      font: Platform.OS === "android" ? "Ubuntu-R" : "Ubuntu",
      strings: Strings_en_GB,
      paymentSuccessfull: false,
      videoNotificationId: 0,
    };
  }

  render() {
    return (
      <UtilsContext.Provider
        value={{
          ...this.state,
          setVideoNotificationId: (videoNotificationId: number) =>
            this.setState({ videoNotificationId }),
          consumedVideoNotification: () =>
            this.setState({ videoNotificationId: 0 }),
          togglePaymentSuccessfull: () =>
            this.setState({
              paymentSuccessfull: !this.state.paymentSuccessfull,
            }),
        }}
      >
        {this.props.children}
      </UtilsContext.Provider>
    );
  }
}
