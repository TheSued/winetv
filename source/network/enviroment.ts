import Routes from "../environments/Routes";
import http from "./http";

export async function getCompanySettings(
  companyId: number,
  platform: string,
  language: string
) {
  const url = `${Routes.CompanySettings}&company=${companyId}&platform=${platform}&test=1&l=${language}`;
  const companySettings = await http.getNoAuth(url);
  // companySettings.show_footer_text = true;
  // companySettings.footer_text =
  //   'bla bla bla bla bla bla bla bla bla bla bla bla bla blab bla';
  return companySettings;
}

export async function getHomepageSettings(companyId: number, auth: boolean) {
  let url = `${Routes.HomepageSettings}&id=${companyId}`;
  let companySettings;
  if (auth) {
    url = `${url}&type=1`;
    companySettings = await http.get(url);
  } else {
    companySettings = await http.getNoAuth(url);
  }

  return companySettings;
}

export async function getCategories() {
  let url = `${Routes.GetCategories}`;
  const categories = await http.get(url);
  return categories;
}
