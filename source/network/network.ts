const userData: {
  token: string;
  companyId: string;
} = {token: '', companyId: ''};

export async function getToken() {
  return userData.token;
}

export function deleteToken() {
  delete userData.token;
}
