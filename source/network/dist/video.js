"use strict";
var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
exports.__esModule = true;
exports.addLike = exports.searchVideos = exports.getVideoInfo = exports.getAttachments = exports.playVideo = exports.updateVideo = exports.getVideoSource = exports.getVideoKeepWatching = exports.GetPreviewUrl = exports.getVideoRelatedPreview = exports.getVideoPreview = exports.getVideoTest = exports.getVideo = void 0;
var Routes_1 = require("../environments/Routes");
var http_1 = require("./http");
var Utils = require("../utils/Video.utils");
function getVideo(type, step) {
    return __awaiter(this, void 0, void 0, function () {
        var url, videos;
        return __generator(this, function (_a) {
            switch (_a.label) {
                case 0:
                    url = Routes_1["default"].GetVideo + "&" + type + "=1&step=" + step;
                    return [4 /*yield*/, http_1["default"].get(url)];
                case 1:
                    videos = _a.sent();
                    return [2 /*return*/, videos];
            }
        });
    });
}
exports.getVideo = getVideo;
function getVideoTest() {
    return __awaiter(this, void 0, void 0, function () {
        var url, videos, _a, _b;
        return __generator(this, function (_c) {
            switch (_c.label) {
                case 0:
                    url = "" + Routes_1["default"].GetVideoTest;
                    _b = (_a = Utils).formatVideoList;
                    return [4 /*yield*/, http_1["default"].get(url)];
                case 1: return [4 /*yield*/, _b.apply(_a, [(_c.sent()) || []])];
                case 2:
                    videos = _c.sent();
                    // console.log("TEST GET VIDEO FOR TEST", JSON.parse(videos[0].markers));
                    return [2 /*return*/, videos];
            }
        });
    });
}
exports.getVideoTest = getVideoTest;
function getVideoPreview() {
    return __awaiter(this, void 0, void 0, function () {
        var url, videoPreview;
        return __generator(this, function (_a) {
            switch (_a.label) {
                case 0:
                    url = "" + Routes_1["default"].GetVideoTestPreview;
                    return [4 /*yield*/, http_1["default"].get(url)];
                case 1:
                    videoPreview = _a.sent();
                    // console.log("PREVIEW", videoPreview);
                    return [2 /*return*/, videoPreview];
            }
        });
    });
}
exports.getVideoPreview = getVideoPreview;
function getVideoRelatedPreview(url) {
    return __awaiter(this, void 0, void 0, function () {
        var formdata, videoPreview;
        return __generator(this, function (_a) {
            switch (_a.label) {
                case 0:
                    formdata = new FormData();
                    formdata.append("url", url);
                    return [4 /*yield*/, http_1["default"].post(Routes_1["default"].GetVideoRelatedPreview, formdata)];
                case 1:
                    videoPreview = _a.sent();
                    return [2 /*return*/, videoPreview];
            }
        });
    });
}
exports.getVideoRelatedPreview = getVideoRelatedPreview;
function GetPreviewUrl(url) {
    return __awaiter(this, void 0, void 0, function () {
        var formdata, videoPreview;
        return __generator(this, function (_a) {
            switch (_a.label) {
                case 0:
                    formdata = new FormData();
                    formdata.append("url", url);
                    return [4 /*yield*/, http_1["default"].post(Routes_1["default"].GetPreviewUrl, formdata)];
                case 1:
                    videoPreview = _a.sent();
                    return [2 /*return*/, videoPreview];
            }
        });
    });
}
exports.GetPreviewUrl = GetPreviewUrl;
function getVideoKeepWatching(step) {
    return __awaiter(this, void 0, void 0, function () {
        var url, videos;
        return __generator(this, function (_a) {
            switch (_a.label) {
                case 0:
                    url = Routes_1["default"].GetVideoKeepWatching + "&step=" + step;
                    return [4 /*yield*/, http_1["default"].get(url)];
                case 1:
                    videos = _a.sent();
                    return [2 /*return*/, videos];
            }
        });
    });
}
exports.getVideoKeepWatching = getVideoKeepWatching;
function getVideoSource(id) {
    return __awaiter(this, void 0, void 0, function () {
        var formdata, videoSource;
        return __generator(this, function (_a) {
            switch (_a.label) {
                case 0:
                    formdata = new FormData();
                    formdata.append("id", id);
                    return [4 /*yield*/, http_1["default"].post(Routes_1["default"].VideoSource, formdata)];
                case 1:
                    videoSource = _a.sent();
                    return [2 /*return*/, videoSource];
            }
        });
    });
}
exports.getVideoSource = getVideoSource;
function updateVideo(id, time, action, end, sp) {
    return __awaiter(this, void 0, void 0, function () {
        var formdata;
        return __generator(this, function (_a) {
            switch (_a.label) {
                case 0:
                    formdata = new FormData();
                    formdata.append("id", id);
                    formdata.append("time", time);
                    formdata.append("action", action);
                    formdata.append("end", end);
                    formdata.append("sp", sp);
                    return [4 /*yield*/, http_1["default"].post(Routes_1["default"].UpdateVideo, formdata)];
                case 1:
                    _a.sent();
                    return [2 /*return*/];
            }
        });
    });
}
exports.updateVideo = updateVideo;
function playVideo(id, time) {
    return __awaiter(this, void 0, void 0, function () {
        var formdata, response;
        return __generator(this, function (_a) {
            switch (_a.label) {
                case 0:
                    formdata = new FormData();
                    formdata.append("id", id);
                    formdata.append("time", time);
                    return [4 /*yield*/, http_1["default"].post(Routes_1["default"].PlayVideo, formdata)];
                case 1:
                    response = (_a.sent()) || [];
                    if (response[0] && response[0].status === "success") {
                        return [2 /*return*/, response[0].action];
                    }
                    return [2 /*return*/];
            }
        });
    });
}
exports.playVideo = playVideo;
function getAttachments(id) {
    return __awaiter(this, void 0, void 0, function () {
        var url, attachments, array;
        return __generator(this, function (_a) {
            switch (_a.label) {
                case 0:
                    url = Routes_1["default"].GetAttachments + "&video=" + id;
                    return [4 /*yield*/, http_1["default"].get(url)];
                case 1:
                    attachments = _a.sent();
                    if (attachments.length) {
                        array = attachments.map(function (item) {
                            var extension = "";
                            for (var i = 1; i < item.file.length; i++) {
                                if (item.file[item.file.length - i] === ".") {
                                    extension = item.file.slice(item.file.length - i + 1, item.file.length);
                                    break;
                                }
                            }
                            return __assign(__assign({}, item), { extension: extension, chars: item.name.length, downloaded: false });
                        });
                        return [2 /*return*/, array];
                    }
                    else {
                        return [2 /*return*/, []];
                    }
                    return [2 /*return*/];
            }
        });
    });
}
exports.getAttachments = getAttachments;
function getVideoInfo(id) {
    return __awaiter(this, void 0, void 0, function () {
        var url, videoInfo;
        return __generator(this, function (_a) {
            switch (_a.label) {
                case 0:
                    url = Routes_1["default"].GetVideo + "&video=" + id;
                    return [4 /*yield*/, http_1["default"].get(url)];
                case 1:
                    videoInfo = (_a.sent()) || [];
                    if (videoInfo[0]) {
                        return [2 /*return*/, videoInfo[0]];
                    }
                    return [2 /*return*/, null];
            }
        });
    });
}
exports.getVideoInfo = getVideoInfo;
function searchVideos(companyId, step, text, categories) {
    return __awaiter(this, void 0, void 0, function () {
        var url, searchResult;
        return __generator(this, function (_a) {
            switch (_a.label) {
                case 0:
                    url = Routes_1["default"].GetVideo + "&step=" + step + "&company=" + companyId + "&search=" + text + "&cat=" + categories.join();
                    return [4 /*yield*/, http_1["default"].get(url)];
                case 1:
                    searchResult = _a.sent();
                    return [2 /*return*/, searchResult];
            }
        });
    });
}
exports.searchVideos = searchVideos;
function addLike(id) {
    return __awaiter(this, void 0, void 0, function () {
        var formdata, likeResult;
        return __generator(this, function (_a) {
            switch (_a.label) {
                case 0:
                    formdata = new FormData();
                    formdata.append("id", id);
                    return [4 /*yield*/, http_1["default"].post(Routes_1["default"].AddLike, formdata)];
                case 1:
                    likeResult = _a.sent();
                    return [2 /*return*/, likeResult[0]];
            }
        });
    });
}
exports.addLike = addLike;
