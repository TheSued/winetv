import Routes from "../environments/Routes";
import http from "./http";
import * as Utils from "../utils/Video.utils";

export async function getVideo(type: string, step: number) {
  const url = `${Routes.GetVideo}&${type}=1&step=${step}`;
  const videos = await http.get(url);
  return videos;
}

export async function getVideoTest() {
  const url = `${Routes.GetVideoTest}`;
  const videos = await Utils.formatVideoList((await http.get(url)) || []);
  // console.log("TEST GET VIDEO FOR TEST", JSON.parse(videos[0].markers));
  return videos;
}

export async function getVideoSubtitles(videoId: any) {
  const url = `${Routes.GetSubtitles + videoId}`;
  const videoSubtitles = await http.get(url);
  return videoSubtitles;
}

export async function getVideoPreview() {
  const url = `${Routes.GetVideoTestPreview}`;
  const videoPreview = await http.get(url);
  // console.log("PREVIEW", videoPreview);
  return videoPreview;
}

export async function getVideoRelatedPreview(url: string) {
  const formdata = new FormData();
  formdata.append("url", url);
  // const videoPreview = await Utils.formatVideoRelatedPreview((await http.get(url)) || []);
  const videoPreview = await http.post(Routes.GetVideoRelatedPreview, formdata);
  return videoPreview;
}

export async function GetPreviewUrl(url: string) {
  const formdata = new FormData();
  formdata.append("url", url);
  // const videoPreview = await Utils.formatVideoRelatedPreview((await http.get(url)) || []);
  const videoPreview = await http.post(Routes.GetPreviewUrl, formdata);
  return videoPreview;
}

export async function getVideoKeepWatching(step: number) {
  const url = `${Routes.GetVideoKeepWatching}&step=${step}`;
  const videos = await http.get(url);
  return videos;
}

export async function getVideoSource(id: number) {
  const formdata = new FormData();
  formdata.append("id", id);
  const videoSource = await http.post(Routes.VideoSource, formdata);
  return videoSource;
}

export async function updateVideo(
  id: number,
  time: number,
  action: string,
  end: number,
  sp: number
) {
  const formdata = new FormData();
  formdata.append("id", id);
  formdata.append("time", time);
  formdata.append("action", action);
  formdata.append("end", end);
  formdata.append("sp", sp);
  await http.post(Routes.UpdateVideo, formdata);
}

export async function playVideo(id: number, time: number) {
  const formdata = new FormData();
  formdata.append("id", id);
  formdata.append("time", time);
  const response = (await http.post(Routes.PlayVideo, formdata)) || [];
  if (response[0] && response[0].status === "success") {
    return response[0].action;
  }
}

export async function getAttachments(id: number) {
  const url = `${Routes.GetAttachments}&video=${id}`;
  const attachments = await http.get(url);
  if (attachments.length) {
    let array = attachments.map((item: any) => {
      let extension = "";
      for (let i = 1; i < item.file.length; i++) {
        if (item.file[item.file.length - i] === ".") {
          extension = item.file.slice(
            item.file.length - i + 1,
            item.file.length
          );
          break;
        }
      }
      return {
        ...item,
        extension: extension,
        chars: item.name.length,
        downloaded: false,
      };
    });
    return array;
  } else {
    return [];
  }
}

export async function getVideoInfo(id: number) {
  const url = `${Routes.GetVideo}&video=${id}`;
  const videoInfo = (await http.get(url)) || [];
  if (videoInfo[0]) {
    return videoInfo[0];
  }
  return null;
}

export async function searchVideos(
  companyId: number,
  step: number,
  text: string,
  categories: string[]
) {
  const url = `${
    Routes.GetVideo
  }&step=${step}&company=${companyId}&search=${text}&cat=${categories.join()}`;
  const searchResult = await http.get(url);
  return searchResult;
}

export async function addLike(id: number) {
  const formdata = new FormData();
  formdata.append("id", id);
  const likeResult = await http.post(Routes.AddLike, formdata);
  return likeResult[0];
}
